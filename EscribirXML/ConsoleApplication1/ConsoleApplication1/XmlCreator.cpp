// ConsoleApplication1.cpp: define el punto de entrada de la aplicación de consola.
//

#include "stdafx.h"
#include "XmlWriter.h"
#include <stdlib.h>
#include <time.h>

int na = 50;//numero de arboles
int nf = 3; //numero de archivos
int Xlength = 256;
int Ylength = 256;
int Xoffset = -128;
int Yoffset = -128;

int main()
{
	XmlWriter xml;
		if (xml.open(".\\test.xml")) {
			xml.writeOpenTag("trees");
			//xml.writeStartElementTag("testEle1");
			//xml.writeString("This is my first tag string!");
			//xml.writeEndElementTag();
			for (int i = 1; i <= na; i += 1) {
				//posicion aleatoria
				int tipo = 1 + rand()%(nf);
				int x = 1 + rand()%Xlength + Xoffset;
				int y = 1 + rand()%Ylength + Yoffset;
				int n1, n2, n3;

				char stringtipo[50];
				char stringx[50];
				char stringy[50];

				n1 = sprintf_s(stringx, "%d", x);
				n2 = sprintf_s(stringy, "%d", y);
				n3 = sprintf_s(stringtipo, "%d", tipo);

				xml.writeOpenTag("tree");
				xml.writeStartElementTag("fichero");
				//xml.writeAttribute("testAtt=\"TestAttribute\"");
				xml.writeString(stringtipo);
				xml.writeEndElementTag();

				xml.writeStartElementTag("x");
				//xml.writeAttribute("testAtt=\"TestAttribute\"");
				xml.writeString(stringx);
				xml.writeEndElementTag();

				xml.writeStartElementTag("y");
				//xml.writeAttribute("testAtt=\"TestAttribute\"");
				xml.writeString(stringy);
				xml.writeEndElementTag();


				xml.writeCloseTag();
			}
				xml.writeCloseTag();
			

			xml.close();
			std::cout << "Success!\n";
		}
		else {
			std::cout << "Error opening file.\n";
		}

    system("pause");
    return 0;
}

