#include "stdafx.h"
#include "SDL.h"
#include "SDL_mixer.h"

#include "AudioGeneral.h"
#include "AudioItemMusica.h"
#include "AudioItemChunk.h"

#define MUSIC_PATH1 "bear_growl.wav"
#define MUSIC_PATH2 "aladdin_cant_believe.wav"

// prototype for our audio callback
// see the implementation for more information
void my_audio_callback(void *userdata, Uint8 *stream, int len);

// variable declarations
static Uint8 *audio_pos; // global pointer to the audio buffer to be played
static Uint32 audio_len; // remaining length of the sample we have to play

int main1();
int main2();
int main3();
int main4();
int main5();
int main6();

int main(int argc, char* argv[]) {

	return main6();
	
}

int main1() {
	// Initialize SDL.
	if (SDL_Init(SDL_INIT_AUDIO) < 0)
		return 1;

	// local variables
	static Uint32 wav_length; // length of our sample
	static Uint8 *wav_buffer; // buffer containing our audio file
	static SDL_AudioSpec wav_spec; // the specs of our piece of music


								   /* Load the WAV */
								   // the specs, length and buffer of our wav are filled
	if (SDL_LoadWAV(MUSIC_PATH1, &wav_spec, &wav_buffer, &wav_length) == NULL) {
		return 1;
	}
	// set the callback function
	//wav_spec.freq = 11025;
	//wav_spec.format = AUDIO_S16;
	//wav_spec.channels = 1;    /* 1 = mono, 2 = stereo */
	//wav_spec.samples = 4096;  /* Good low-latency value for callback */
	wav_spec.callback = my_audio_callback;
	wav_spec.userdata = NULL;
	// set our global static variables
	audio_pos = wav_buffer; // copy sound buffer
	audio_len = wav_length; // copy file length

							/* Open the audio device */
	if (SDL_OpenAudio(&wav_spec, NULL) < 0) {
		fprintf(stderr, "Couldn't open audio: %s\n", SDL_GetError());
		exit(-1);
	}

	/* Start playing */
	SDL_PauseAudio(0);

	// wait until we aren't playing
	while (audio_len > 0) {
		SDL_Delay(100);
	}

	// shut everything down
	SDL_CloseAudio();
	SDL_FreeWAV(wav_buffer);
	return 0;
}
void my_audio_callback(void *userdata, Uint8 *stream, int len) {

	if (audio_len == 0)
		return;

	len = (len > audio_len ? audio_len : len);
	//SDL_memcpy (stream, audio_pos, len); 					// simply copy from one buffer into the other
	SDL_MixAudio(stream, audio_pos, len, SDL_MIX_MAXVOLUME);// mix from one buffer into another

	audio_pos += len;
	audio_len -= len;
}

#define ID_CUERVOS "cuervos"
#define ID_PLAYIT "playit"
#define WAV_PATH "Sonidos/Crows.wav"
#define MUS_PATH "Sonidos/play_it.wav"
// Our wave file
Mix_Chunk *wave = NULL;
// Our music file
Mix_Music *music = NULL;
int main2() {
	// Initialize SDL.
	if (SDL_Init(SDL_INIT_AUDIO) < 0)
		return -1;

	//Initialize SDL_mixer 
	if (Mix_OpenAudio(22050, MIX_DEFAULT_FORMAT, 2, 4096) == -1)
		return -1;

	// Load our sound effect
	wave = Mix_LoadWAV(MUS_PATH);
	if (wave == NULL)
		return -1;

	// Load our music
	music = Mix_LoadMUS(WAV_PATH);
	if (music == NULL)
		return -1;

	if (Mix_PlayChannel(-1, wave, 0) == -1)
		return -1;

	if (Mix_PlayMusic(music, 0) == -1) //�ltimo argumento: veces que se repite (-1=infinitas veces)
		return -1;

	while (Mix_PlayingMusic());

	// clean up our resources
	Mix_FreeChunk(wave);
	Mix_FreeMusic(music);

	// quit SDL_mixer
	Mix_CloseAudio();

	return 0;
}

//igual que main2() pero con clases creadas
int main3() {
	AudioGeneral AG;

	AudioItemMusica music_playit("Sonidos/play_it.wav");
	//AudioItemMusica music_puzzlegame("Sonidos/PuzzleGameLooping.mp3");
	AudioItemChunk chunk_crows("Sonidos/Crows.wav");

	chunk_crows.play(1);
	music_playit.play(1);

	//AudioItem ai2(MUS_PATH, true);
	//ai2.playAudio(1);
	//ai1.playAudio(2);
	
	while (1) {
		
	}
}


#define MY_COOL_MP3 "Sonidos/Rain_Background-Mike_Koenig-1681389445.mp3"
#define MY_MP3 "Sonidos/PuzzleGameLooping.mp3"
//para reproducir mp3 (funciona con Rain_Background-Mike_Koenig-1681389445.mp3)
int main4() {
	int result = 0;
	int flags = MIX_INIT_MP3;

	if (SDL_Init(SDL_INIT_AUDIO) < 0) {
		printf("Failed to init SDL\n");
		system("pause");
		return 1;
	}

	if (flags != (result = Mix_Init(flags))) {
		printf("Could not initialize mixer (result: %d).\n", result);
		printf("Mix_Init: %s\n", Mix_GetError());
		system("pause");
		return 2;
	}

	Mix_OpenAudio(22050, AUDIO_S16SYS, 2, 1024);
	Mix_Music *music = Mix_LoadMUS(MY_COOL_MP3); //////Falla con PuzzleGameLooping.mp3
	Mix_PlayMusic(music, 1);

	while (!SDL_QuitRequested()) {
		SDL_Delay(250);
	}

	while (1) {
	}

	Mix_FreeMusic(music);
	SDL_Quit();
	return 0;
}
//igual: para reproducir mp3 (funciona con Rain_Background-Mike_Koenig-1681389445.mp3)
int main5() {
	Mix_Music *music = 0;
	int ret = 0;

	ret = SDL_Init(SDL_INIT_AUDIO);
	if (ret) {
		fprintf(stderr, "%s\n", SDL_GetError());
		system("pause");
		return 1;
	}

	ret = Mix_Init(MIX_INIT_MP3);
	if ((ret & MIX_INIT_MP3) != MIX_INIT_MP3) {
		fprintf(stderr, "%s\n", SDL_GetError());
		system("pause");
		return 2;
	}

	ret = Mix_OpenAudio(22050, MIX_DEFAULT_FORMAT, 2, 4096);
	if (ret < 0) {
		fprintf(stderr, "%s\n", SDL_GetError());
		system("pause");
		return 3;
	}

	music = Mix_LoadMUS(MY_COOL_MP3);
	if (!music) {
		fprintf(stderr, "%s\n", SDL_GetError());
		system("pause");
		return 4;
	}

	ret = Mix_PlayMusic(music, 1);
	if (ret == -1) {
		fprintf(stderr, "%s\n", SDL_GetError());
		system("pause");
		return 5;
	}

	SDL_Delay(5000);

	Mix_FreeMusic(music);
	Mix_CloseAudio();
	Mix_Quit();
	SDL_Quit();
}
//igual: para reproducir mp3 (funciona con Rain_Background-Mike_Koenig-1681389445.mp3)
int main6() {

	if (SDL_Init(SDL_INIT_EVERYTHING) != 0) {
		printf("Cannot init SDL.\n");
		return -1;
	}

	int code = Mix_Init(MIX_INIT_MP3);
	if (code & MIX_INIT_MP3) {
		printf("MP3 libraries loaded fine: %d\n", code);
	}

	if (Mix_OpenAudio(MIX_DEFAULT_FREQUENCY, MIX_DEFAULT_FORMAT, 2, 4096) != 0) {
		printf("Can not open audio.\n");
		return -1;
	}


	Mix_Music *music = NULL;
	music = Mix_LoadMUS(MY_COOL_MP3);

	// Check load
	if (music == NULL) {
		printf("Mix_Music is empty! %d\n", Mix_GetError());
		return -1;
	}

	// Just for sure that music is not muted
	Mix_VolumeMusic(128);

	if (Mix_PlayMusic(music, 1) != 0) {
		printf("Error while playing\n");
		return -1;
	}

	// Small delay to get time to listen to that beautiful music :)
	SDL_Delay(1000 * 20);

	// Free music
	Mix_FreeMusic(music);

	// End of Mix library
	Mix_CloseAudio();
	Mix_Quit();

	// End of SDL library
	SDL_Quit();
}