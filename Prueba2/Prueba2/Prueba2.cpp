// Prueba2.cpp: define el punto de entrada de la aplicaci�n de consola.
//
//Clase 26/09/2016

#include "stdafx.h"
#include "MovilRigido.h"
#include "MovilBlando.h"

const double inc_t = 0.01;

void principal();
void principal2();
void principal3();

int main()
{
	principal3();


	//principal();

	system("pause"); //pide presionar una tecla para continuar
	Sleep(1000);

    return 0; //aqu� llama a los destructores
}

void principal3() {
	vector< shared_ptr<Movil> > vector;

	vector.push_back( shared_ptr<Movil>( new MovilBlando() ) );
	vector.push_back( shared_ptr<Movil>( new MovilRigido() ) );

	auto ite = vector.begin();
	auto end = vector.end();

	for (; ite != end; ite++) {
		auto i = ite->get();
		cout << "caso)\t" << "X = " << i->getX() << " Y = " << i->getY() << "\t->\t";
		i->simular(inc_t);
		cout << "X = " << i->getX() << " Y = " << i->getY() << endl;
	}

}

void principal2() {
	vector<Movil*> vector;

	vector.push_back( new MovilBlando() );
	vector.push_back(new MovilRigido());

	auto ite = vector.begin();
	auto end = vector.end();

	for (; ite != end; ite++) {
		cout << "caso)\t" << "X = " << (*ite)->getX() << " Y = " << (*ite)->getY() << "\t->\t";
		(*ite)->simular(inc_t);
		cout << "X = " << (*ite)->getX() << " Y = " << (*ite)->getY() << endl;
	}

	for (; ite != end; ite++) {
		delete (*ite);
	}
	
}

void principal() {
	MovilRigido m1, m2; //llamada a los constructores
	vector<MovilRigido> v;

	MovilRigido mo;
	v.push_back(mo);
	mo.setX(4.0);
	v.push_back(mo);

	auto ite = v.begin(); //auto averigua el tipo y lo pone;	ite=iterador
	auto end = v.end();
	for (; ite != end; ite++) {
		ite->simular(inc_t);
		cout << "x = " << ite->getX() << " y = " << ite->getY() << endl;
	}
	//cout << "x = " << v.at(3).getX() << " y = " << v.at(3).getX() << endl; //no hay tercer elemento, luego falla la ejecuci�n


	//v.pop_back();



	//m1.setX(3.0);
	//m1.setX(m1.getX() + 3 * inc_t);
	m1.simular(inc_t);

	//printf("X = %f\n", m1.getX());
	cout << "x =" << m1.getX() << endl;
}

