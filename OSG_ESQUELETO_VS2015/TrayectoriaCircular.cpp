#include "TrayectoriaCircular.h"


TrayectoriaCircular::TrayectoriaCircular()
{
}

TrayectoriaCircular::TrayectoriaCircular(double a, double b, double c, double r, double t, double p)
{
	centro.setX(a);
	centro.setY(b);
	centro.setZ(c);
	radio = r;
	theta = t;
	phi = p;
}


TrayectoriaCircular::~TrayectoriaCircular()
{
}
