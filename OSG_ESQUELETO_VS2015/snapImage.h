#pragma once

#include <osgUtil/Optimizer>
#include <osgDB/ReadFile>

#include <osgViewer/Viewer>
#include <osgViewer/CompositeViewer>

#include <osgGA/TrackballManipulator>

#include <osg/Material>
#include <osg/Geode>
#include <osg/BlendFunc>
#include <osg/Depth>
#include <osg/PolygonOffset>
#include <osg/MatrixTransform>
#include <osg/Camera>
#include <osg/RenderInfo>

#include <osgDB/WriteFile>

#include <osgText/Text>

#include "files.h"

struct SnapImage : public osg::Camera::DrawCallback
{
	SnapImage(const std::string& filename) :
		_filename(filename),
		_snapImage(false)
	{
		_image = new osg::Image;
		_number = 1;
	}

	virtual void operator () (osg::RenderInfo& renderInfo) const
	{

		if (!_snapImage) return;

		/*std::cout << "Camera callback" << std::endl;*/

		osg::Camera* camera = renderInfo.getCurrentCamera();
		osg::Viewport* viewport = camera ? camera->getViewport() : 0;

		//std::cout << "Camera callback " << camera << " " << viewport << std::endl;

		if (viewport && _image.valid())
		{
			_image->readPixels(int(viewport->x()), int(viewport->y()), int(viewport->width()), int(viewport->height()),
				GL_RGBA,
				GL_UNSIGNED_BYTE);

			std::string s = _filename;
			do {
				if (_number<10)
					s = _filename + "00" + std::to_string(_number);
				else if (_number<100)
					s = _filename + "0" + std::to_string(_number);
				else
					s = _filename + std::to_string(_number);
				s = s + ".png";
				_number++;
			} while (fileExists(s));

			/*while (fileExists(_filename)) {
				std::string* s2; //para modificar _filename que es constante
				s2 = (std::string*)(&_filename);
				*s2 += "0";
			}*/

			osgDB::writeImageFile(*_image, s);
			
			printf("SCREENSHOT: %s\n", s.c_str());
			//std::cout << "Taken screenshot, and written to '" << _filename << "'" << std::endl;
		}

		_snapImage = false;
	}

	std::string                         _filename;
	mutable unsigned int				_number;
	mutable bool                        _snapImage;
	mutable osg::ref_ptr<osg::Image>    _image;
};

struct SnapeImageHandler : public osgGA::GUIEventHandler
{

	SnapeImageHandler(int key, SnapImage* si) :
		_key(key),
		_snapImage(si) {}

	bool handle(const osgGA::GUIEventAdapter& ea, osgGA::GUIActionAdapter&)
	{
		if (ea.getHandled()) return false;

		switch (ea.getEventType())
		{
		case(osgGA::GUIEventAdapter::KEYUP):
		{
			if (ea.getKey() == _key)
			{
				//std::cout  << "event handler" << std::endl;
				_snapImage->_snapImage = true;
				return true;
			}

			break;
		}
		default:
			break;
		}

		return false;
	}

	int                     _key;
	osg::ref_ptr<SnapImage> _snapImage;
};






