#include "Toast.h"



Toast::Toast()
{
}

void Toast::addMessage(std::string s, unsigned int t)
{
	Data d(s, t);
	list.push_back(d);
}

std::string Toast::getMessages()
{
	std::string s = "";
	for (std::list<Data>::iterator it = list.begin(); it != list.end(); it++) {
		unsigned int t1 = ::GetCurrentTime();
		unsigned int t0 = it->t0;
		unsigned int T = it->time;
		if ((t1 - t0) < (T * 1000)) {
			s += it->message;
			s += "\n";
		}
		else
			list.erase(it);
	}
	return s;
}


Toast::~Toast()
{
}
