#pragma once

#include "Punto.h"

class TrayectoriaCircular
{
	protected:
		Punto centro;
		double radio;
		double theta;
		double phi;
	public:
		TrayectoriaCircular();
		TrayectoriaCircular(double a, double b, double c, double r, double t, double p);
		virtual ~TrayectoriaCircular();
};

