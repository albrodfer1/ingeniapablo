#pragma once

#include <string>
#include <list>
#include "atltime.h"

struct Data {
	std::string message;
	unsigned int time;
	unsigned int t0;
	Data::Data(std::string s, unsigned int t){
		message = s;
		time = t;
		t0 = ::GetCurrentTime();
	}
};

class Toast
{
private:
	std::list<Data> list;
public:
	Toast();
	void addMessage(std::string s, unsigned int t=3);
	std::string getMessages();
	~Toast();
};

