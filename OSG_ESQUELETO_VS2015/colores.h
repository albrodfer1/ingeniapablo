#pragma once

#include <osg/Vec4>

//tabla con muchos colores: http://prideout.net/archive/colors.php

osg::Vec4 COLOR_NEGRO(0.0f, 0.0f, 0.0f, 1.0f);
osg::Vec4 COLOR_GRIS_OSCURO(0.25f, 0.25f, 0.25f, 1.0f);
osg::Vec4 COLOR_GRIS(0.5f, 0.5f, 0.5f, 1.0f);
osg::Vec4 COLOR_GRIS_CLARITO(0.75f, 0.75f, 0.75f, 1.0f);
osg::Vec4 COLOR_BLANCO(1.0f, 1.0f, 1.0f, 1.0f);

osg::Vec4 COLOR_ROJO_OSCURO(0.25f, 0.0f, 0.0f, 1.0f);
osg::Vec4 COLOR_ROJO(0.5f, 0.0f, 0.0f, 1.0f);
osg::Vec4 COLOR_ROJO_CLARITO(0.75f, 0.0f, 0.0f, 1.0f);

osg::Vec4 COLOR_VERDE_OSCURO(0.0f, 0.25f, 0.0f, 1.0f);
osg::Vec4 COLOR_VERDE(0.0f, 0.5f, 0.0f, 1.0f);
osg::Vec4 COLOR_VERDE_CLARITO(0.0f, 0.75f, 0.0f, 1.0f);

osg::Vec4 COLOR_AZUL_OSCURO(0.0f, 0.0f, 0.25f, 1.0f);
osg::Vec4 COLOR_AZUL(0.0f, 0.0f, 0.5f, 1.0f);
osg::Vec4 COLOR_AZUL_CLARITO(0.0f, 0.0f, 0.75f, 1.0f);

osg::Vec4 COLOR_AMARILLO(1.0f, 1.0f, 0.0f, 1.0f);
osg::Vec4 COLOR_CYAN(0.0f, 1.0f, 1.0f, 1.0f);
osg::Vec4 COLOR_MAGENTA(1.0f, 0.0f, 1.0f, 1.0f);

osg::Vec4 COLOR_MARRON(0.60f, 0.40f, 0.12f, 1.0f);
osg::Vec4 COLOR_NARANJA(0.98f, 0.625f, 0.12f, 1.0f);
osg::Vec4 COLOR_ROSA(0.98f, 0.04f, 0.7f, 1.0f);
osg::Vec4 COLOR_MORADO(0.60f, 0.40f, 0.70f, 1.0f);

osg::Vec4 COLOR_SALMON(1.0, 0.627, 0.478, 1.0);

