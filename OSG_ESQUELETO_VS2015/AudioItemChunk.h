#pragma once

#include <string>
#include "SDL.h"
#include "SDL_mixer.h"

#include <stdio.h>

class AudioItemChunk
{
private:
	unsigned int error;
	std::string path;
	Mix_Chunk *wave = NULL;
public:
	AudioItemChunk();
	AudioItemChunk(std::string p);
	~AudioItemChunk();
	unsigned int getError() { return error; }
	void setPath(std::string str);
	std::string getPath() { return path; }
	void play(int veces);
};

