#pragma once

//para c�lculos
#define PI 3.14159265358979323846

#define G -9.81

//de la partida
#define TOTAL_TIME 180 //tiempo de duraci�n de la partida en segundos

//para la c�mara
#define CAM_DIST 7
#define CAM_DIST_VAR 0.51 //0.01 para evitar que distancia(Yo-Camara d� nula)
#define CAM_ANG 15 //grados

//para el humano
#define X0 11.0
#define Y0 -27.0
#define Z0 0
#define DIRECCION0 -20.0
#define CTE_GIRO 0.05
#define CTE_AVANCE 0.2
#define VEL_ANDAR 5
#define DESNIVEL 0.1
#define DISTANCIA_A_OTROS 3.0

//para el heightmap
#define HEIGHTMAP_ARCHIVO "./Data/Texturas/Heightmap.png"
#define HEIGHTMAP_ARCHIVO_NORMALES "./Data/Texturas/heightmap_normals.png"
#define HEIGHTMAP_ARCHIVO_TEXTURAS "./Data/Texturas/heightmap_rgb.png"
#define HEIGHTMAP_ALTURA 25
#define NUM_PUNTOS 256
#define SEPARACION_PUNTOS 1

//para el mapa
#define MAPA_ARCHIVO "./Mapas/mapa2.png"

//para el rayo
#define LONGITUD_RAYO 0.5

//para la trayectoria del sol
#define CENTRO_X 0.0
#define CENTRO_Y 0.0
#define CENTRO_Z 0.0
#define RADIO 1200.0
#define VEL_ANG_PHI 0.1
#define VEL_ANG_THETA 0.0

//para el WindowManager
const unsigned int MASK_2D = 0xF0000000;
const unsigned int MASK_3D = 0x0F000000;

//para los di�logos
#define MAX_POR_LINEA 45


