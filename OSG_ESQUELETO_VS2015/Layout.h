#pragma once

#include "stdafx.h"

#include <osg/Node>
#include <osg/Geometry>
#include <osg/Notify>
#include <osg/MatrixTransform>
#include <osg/Texture2D>
#include <osg/Billboard>
#include <osg/LineWidth>

#include <osg/Material>
#include <osg/StateSet>
#include <osg/TexMat>
#include <osg/ShapeDrawable>

#include <osgDB/Registry>
#include <osgDB/ReadFile>

#include <osgViewer/Viewer>

#include <Windows.h>
#include <math.h>
#include <iostream>
#include "ObjetoMovil.h"
	#include "stdafx.h"
	#include "FuncionesAyuda.h"
osg::MatrixTransform*escenario (std::string dir, float posx, float posy, float posz, float posfi, float esc);
osg::MatrixTransform*escenario_duplicado (osg::Node* nodosuelo, float posx, float posy, float posz, float posfi, float esc);// Crea matriz 