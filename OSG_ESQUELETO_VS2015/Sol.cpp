#include "Sol.h"


Solazo::Solazo(double a, double b, double c, double r, double p, double t) : TrayectoriaCircular(a, b, c, r, p, t)  {
	posicion.setX(centro.getX() + radio * cos(theta) * sin(phi));
	posicion.setY(centro.getY() + radio * sin(theta) * sin(phi));
	posicion.setZ(centro.getZ() + radio * cos(phi));
}

void Solazo::move(double var_t)
{
	phi = phi + VEL_ANG_PHI*var_t;
	theta = theta + VEL_ANG_THETA*var_t;
	posicion.setX(  centro.getX() + radio * cos(phi) * cos(theta)  );
	posicion.setY(  centro.getY() + radio * cos(phi) * sin(theta)  );
	posicion.setZ(  centro.getZ() + radio * sin(phi)               );
}

double Solazo::getDayDuration()
{
	if (VEL_ANG_PHI == 0.0 && VEL_ANG_PHI == 0.0)
		return -1000;
	else if (VEL_ANG_PHI == 0.0)
		return 2 * PI / (VEL_ANG_THETA+0.001);//+0.001 para que lo acepte el compilador
	else if (VEL_ANG_THETA == 0.0)
		return 2 * PI / VEL_ANG_PHI;
	else
		return -1;
}

std::string Solazo::tellDayDuration()
{
	double t = getDayDuration();
	if (t == -1000)
		return "infinito";
	else if(t==-1)
		return "el sol no recorre un c�rculo";
	else {
		std::string s="";
		s.append(std::to_string(int(t)));
		s.append("s (");
		s.append(std::to_string(int(t/60)));
		s.append("min)");
		return s;
	}
}
