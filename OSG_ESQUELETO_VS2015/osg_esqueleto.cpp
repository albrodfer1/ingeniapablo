﻿#pragma once

#define WIN32_LEAN_AND_MEAN    // stops windows.h including winsock.h
#include <winsock2.h>

#include "stdafx.h"
#include "FuncionesAyuda.h"

#include <string>
#include <math.h>
//#include <iostream>
#include <fstream>
#include <vector>
#include <array>
#include <iterator>

#include <osg/AutoTransform>
#include <osg/Billboard>
#include <osg/BlendFunc>
#include <osg/Depth>
#include <osg/Geode>
#include <osg/Geometry>
#include <osg/Group>
#include <osg/Light>
#include <osg/LightSource>
#include <osg/LineWidth>
#include <osg/Material>
#include <osg/MatrixTransform>
#include <osg/Node>
#include <osg/Notify>
#include <osg/PositionAttitudeTransform>
#include <osg/Projection>
#include <osg/ShapeDrawable>
#include <osg/StateSet>
#include <osg/TexEnv>
#include <osg/TexGen>
#include <osg/TexMat>
#include <osg/Texture2D>
#include <osgText/Text>

#include <osgDB/ReadFile>
#include <osgDB/Registry>
#include <osgDB/WriteFile>

#include <osgGA/TrackballManipulator>
#include <osgGA/StateSetManipulator>

#include <osgShadow/ShadowedScene>
#include <osgShadow/ShadowVolume>
#include <osgShadow/ShadowTexture>
#include <osgShadow/ShadowMap>
#include <osgShadow/SoftShadowMap>
#include <osgShadow/ParallelSplitShadowMap>
#include <osgShadow/LightSpacePerspectiveShadowMap>
#include <osgShadow/StandardShadowMap>
#include <osgShadow/ViewDependentShadowMap>

#include <osgViewer/Viewer>
#include <osgViewer/ViewerEventHandlers>

#include <osgWidget/WindowManager>
#include <osgWidget/ViewerEventHandlers>
#include <osgWidget/Box>

#include "constantes.h"
#include "Punto.h"
#include "Humano.h"
#include "Sol.h"
#include "Toast.h"
#include "Dialogos.h"

#include "rapidxml-1.13/rapidxml.hpp"
#include <osg/LineSegment>
#include <osgUtil/IntersectVisitor>

#include "SDL.h"
#include "SDL_mixer.h"
#include "AudioGeneral.h"
#include "AudioItemMusica.h"
#include "AudioItemChunk.h"
AudioGeneral AG;
AudioItemChunk chunk_playit;
AudioItemChunk chunk_crows;

#include "snapImage.h"
#include "nameInsertedHandler.h"
#include "WidgetMenu.h"
#include "WidgetMap.h"

osgViewer::Viewer viewer;

enum FASE {MENU_INICIO, INSERTAR_NOMBRE, JUGAR, GUARDAR, REINICIAR_PARTIDA, SALIR, MAPA};
FASE FASE_GENERAL = JUGAR;
std::string FASE_JUEGO = "1";
std::string FASE_JUEGOprev = "1";
std::string NOMBRE;

int numDialogosJack = 0;
std::string dialogoPersonaje;
std::string dialogoFase;
std::string respuesta1fase = FASE_JUEGO;
std::string respuesta2fase = FASE_JUEGO;
std::string respuesta3fase = FASE_JUEGO;

osgWidget::Window* boxMap;
osgWidget::Widget* widgetMap;

std::string str_mostrar = "duracion del dia: ";
std::string str_coordMouse = "";
std::string str_tiempoRestante = "te quedan: ";

double tiempoCompletado = 0; //en segundos

int screenLength = 0;
int screenHeight = 0;
double margin = 0.1; //en tanto por 1
int windowLength = 800;
int windowHeight = 600;

struct control_teclado
{
	double x;
	double y;

} click;
bool clickIzquierdo = false;
double anguloCamaraVertical0 = 0;
double anguloCamaraHorizontal0 = 0;
double anguloCamaraVertical = 0;
double anguloCamaraHorizontal = 0;
double anguloCamaraVertical_1aPersona = 0;
double anguloCamaraHorizontal_1aPersona = 0;

Toast toast_global;
Solazo Sol(CENTRO_X, CENTRO_Y, CENTRO_Z, RADIO, 90.0, 0.0);
double Tdia = 0;
//Punto Sol(-70, -50, 20);
Humano Yo(X0, Y0, Z0, DIRECCION0);
Humano Jack(X0+2, Y0+2, Z0+2, DIRECCION0);
Punto Camara(0, 0, 0);
int scroll_times = 3;
bool cam_detras = true;
bool cam_detras_en_cabeza = false;
void calcularPosCamara() {
	double anguloHorizontal = 0;
	double anguloVertical = 0;
	if (scroll_times == 0) { //1a persona
		anguloHorizontal = Yo.getDireccion();
		anguloVertical = anguloCamaraVertical_1aPersona;
	}
	else {
		bool mov = Yo.enMovimiento();
		if (mov && anguloCamaraHorizontal != 0)
			anguloCamaraHorizontal *= 0.95;
		/*if (mov && anguloCamaraVertical != 0)
			anguloCamaraVertical *= 0.95;*/
		anguloHorizontal = Yo.getDireccion() + anguloCamaraHorizontal;
		anguloVertical = 3*PI/180*scroll_times + anguloCamaraVertical;
	}
	//double cam_dist = CAM_DIST - CAM_DIST_VAR*scroll_times;
	double cam_dist = pow(1.7, scroll_times) - 1.2;
	if(cam_dist<0) cam_detras=false;
	else cam_detras=true;
	if (cam_dist<0.5 && cam_dist>=0) cam_detras_en_cabeza = true;
	else cam_detras_en_cabeza = false;
	Camara.setX(Yo.getX() - cam_dist*cos(anguloVertical)*cos(anguloHorizontal));
	Camara.setY(Yo.getY() - cam_dist*cos(anguloVertical)*sin(anguloHorizontal));
	Camara.setZ(Yo.getZ() + cam_dist*sin(anguloVertical));
}

/*#define WAV_PATH "Sonidos/Crows.wav"
#define MUS_PATH "Sonidos/play_it.wav"
Mix_Chunk *wave = NULL;
Mix_Music *music = NULL;
int audio_suena() {
	// Initialize SDL.
	if (SDL_Init(SDL_INIT_AUDIO) < 0)
		return -1;

	//Initialize SDL_mixer 
	if (Mix_OpenAudio(22050, MIX_DEFAULT_FORMAT, 2, 4096) == -1)
		return -1;

	// Load our sound effect
	wave = Mix_LoadWAV(WAV_PATH);
	if (wave == NULL)
		return -1;

	// Load our music
	music = Mix_LoadMUS(MUS_PATH);
	if (music == NULL)
		return -1;

	if (Mix_PlayChannel(-1, wave, 0) == -1)
		return -1;

	if (Mix_PlayMusic(music, 0) == -1) //último argumento: veces que se repite (-1=infinitas veces)
		return -1;

	while (Mix_PlayingMusic());

	// clean up our resources
	Mix_FreeChunk(wave);
	Mix_FreeMusic(music);

	// quit SDL_mixer
	Mix_CloseAudio();

	return 0;
}*/


//osg::ref_ptr<osg::Image> captura = new osg::Image;
//void capturarPantalla() {
//	osg::Viewport* viewport = viewer.getCamera()->getViewport();
//	std::string filename = "SnapImage.png";
//	//osg::ref_ptr<osg::Image> captura = new osg::Image;
//	if (viewport && captura.valid())
//	{
//		printf("capturando pantalla\n");
//		printf("tamaño de imagen: %d x %d\n", int(viewport->width()), int(viewport->height()));
//		captura->readPixels(int(viewport->x()), int(viewport->y()), int(viewport->width()), int(viewport->height()), GL_RGBA, GL_UNSIGNED_BYTE);
//		if (osgDB::writeImageFile(*captura, filename))
//		{
//			printf("imagen guardada\n");
//		}
//		else
//		{
//			printf("error al guardar la imagen\n");
//		}
//	}
//	else
//		printf("imposible hacer captura\n");
//}

void GetDesktopResolution(int& horizontal, int& vertical)
{
	RECT desktop;
	// Get a handle to the desktop window
	const HWND hDesktop = GetDesktopWindow();
	// Get the size of screen to the variable desktop
	GetWindowRect(hDesktop, &desktop);
	// The top left corner will have coordinates (0,0)
	// and the bottom right corner will have coordinates
	// (horizontal, vertical)
	horizontal = desktop.right;
	vertical = desktop.bottom;
}

//hay que comprobar que funcione
void GetWindowResolution(unsigned int& width, unsigned int& height)
{

	/*unsigned int width = 0, height = 0;
	Producer::RenderSurface* rendersurface = viewer.getCamera()->getRenderSurface();
	rendersurface->getWindowRectangle(x, y, width, height);
	printf("Window size & pos: %dx%d at (%d,%d)\n", width, height, x, y);*/

	osg::GraphicsContext::WindowingSystemInterface* wsi = osg::GraphicsContext::getWindowingSystemInterface();
	if (!wsi) {
		printf("Error, no WindowSystemInterface available, cannot create windows.\n");
		return;
	}
	wsi->getScreenResolution(osg::GraphicsContext::ScreenIdentifier(0), width, height);
}

void setWindowTitle(osgViewer::Viewer& viewer, std::string title) {
	
	osg::ref_ptr< osg::GraphicsContext::Traits > traits =
		new osg::GraphicsContext::Traits(*viewer.getCamera()->getGraphicsContext()->getTraits());
	
	traits->windowName = title;
	
	osg::ref_ptr< osg::GraphicsContext > gc = osg::GraphicsContext::createGraphicsContext(traits.get());
	osg::ref_ptr< osg::Camera > cam = new osg::Camera(*viewer.getCamera());
	cam->setGraphicsContext(gc);
	viewer.setCamera(cam.get());
}

class SimulacionKeyboardEvent: public osgGA::GUIEventHandler
{
protected:
public:
	SimulacionKeyboardEvent::SimulacionKeyboardEvent()
	{		
	};
	struct control_teclado
	{
		double x;
		double y;
		
	} tecla_pulsada;
	
	virtual bool SimulacionKeyboardEvent::handle(const osgGA::GUIEventAdapter& ea,osgGA::GUIActionAdapter&)
    {
	    
		int tecla = ea.getKey();
		int evento = ea.getEventType();
		int shift = ea.getModKeyMask();
		int button = ea.getButton();
		int scroll = ea.getScrollingMotion();
		tecla_pulsada.x = ea.getX(); //¡devuelve enteros, no float!
		tecla_pulsada.y = ea.getY(); //¡devuelve enteros, no float!
		
		//printf("evento: %d\n", evento);
		switch (evento)
		{
			//std::cout << evento << endl;
			case(osgGA::GUIEventAdapter::KEYDOWN):	//Evento pulsacion tecla
			{
				printf("tecla %c (%d) pulsada\n", tecla, tecla);
				switch (tecla)
				{
					case 'w':
						{
							static double t1 = 0; //por ser static, esta linea solo se ejecuta una vez
							double t2 = ::GetCurrentTime();
							if ((t2 - t1) < 1000 && Yo.getAvance() == false && Yo.getCorrer() == false) {
								Yo.setCorrer(true);
								Yo.setRetroceso(false);
							}
							else if (Yo.getCorrer() == false){
								Yo.setAvance(true);
								Yo.setRetroceso(false);
								t1 = t2;
							}
						}
						break;
					case 's':
						Yo.setAvance(false);
						Yo.setRetroceso(true);
						break;
					case 'd':
						//Yo.girar(-GIRO);
						Yo.setGiroDer(true);
						Yo.setGiroIzq(false);
						break;
					case 'a':
						//Yo.girar(GIRO);
						Yo.setGiroDer(false);
						Yo.setGiroIzq(true);
						break;
					case 32: //barra espaciadora
						Yo.startSalto(true);
						break;
					case 65451://+
						if (cam_detras && !cam_detras_en_cabeza)
							if (scroll_times == 0) {
								anguloCamaraHorizontal_1aPersona = 0;
								anguloCamaraVertical_1aPersona = 0;
							}
							else if (scroll_times > 0) {
								scroll_times--;
								if (scroll_times == 0)
									viewer.requestWarpPointer(windowLength / 2, windowHeight / 2);
							}
						break;
					case 65453://-
						scroll_times++;
						break;
					//case 'p':
						//reservado para capturas de pantalla
						//break;
					case 'l':
						//osgViewer::GraphicsWindow graphicsWindow;  //set mouse position???
						//graphicsWindow.requestWarpPointer(0.5, 0.5);
						viewer.requestWarpPointer(windowLength/2, windowHeight/2);
						break;
					case 'n':
						toast_global.addMessage("Hola", 4);
						break;
					default:
						break;
				}
				break;
			}
			case(osgGA::GUIEventAdapter::KEYUP):	// Se deja de pulsar una tecla
			{
				printf("tecla %c levantada\n", tecla);
				switch (tecla)
				{
				case 'w':
					//Yo.avanzar(AVANCE);
					Yo.setAvance(false);
					Yo.setCorrer(false);
					break;
				case 's':
					//Yo.avanzar(-AVANCE);
					Yo.setRetroceso(false);
					break;
				case 'd':
					//Yo.girar(-GIRO);
					Yo.setGiroDer(false);
					break;
				case 'a':
					//Yo.girar(GIRO);
					Yo.setGiroIzq(false);
					break;
				case 'b':
					chunk_playit.play(1);
					break;
				case 'm':
				{
					static bool mapaAgrandado = false;
					if (mapaAgrandado) {
						widgetMap->setSize(200, 200);
						boxMap->resize(200, 200);
						mapaAgrandado = false;
					}
					else {
						widgetMap->setSize(400, 400);
						boxMap->resize(400, 400);
						mapaAgrandado = true;
					}
					break;
				}
				default:
					break;
					/*case 'c':
						tecla_pulsada.x += 1;
						break;
					case 'v':
						tecla_pulsada.x -= 1;
						break;
					default:
						break;*/
				}
				break;
			}
			case(osgGA::GUIEventAdapter::SCROLL):
			{
				printf("scroll motion: %d\n", scroll);
				switch (scroll)
				{
					case osgGA::GUIEventAdapter::SCROLL_UP: //=3
						if(cam_detras && !cam_detras_en_cabeza)
							if (scroll_times == 0) {
								anguloCamaraHorizontal_1aPersona = 0;
								anguloCamaraVertical_1aPersona = 0;
							}
							else if (scroll_times > 0) {
								scroll_times--;
								if(scroll_times==0)
									viewer.requestWarpPointer(windowLength / 2, windowHeight / 2);
							}
						break;
					case osgGA::GUIEventAdapter::SCROLL_DOWN: //=4
						scroll_times++;
						break;
				}
				break;
			}
			case(osgGA::GUIEventAdapter::PUSH):
			{
				printf("push\n");
				switch (button)
				{
					case osgGA::GUIEventAdapter::LEFT_MOUSE_BUTTON:
						clickIzquierdo = true;
						click.x = tecla_pulsada.x;
						click.y = tecla_pulsada.y;
						anguloCamaraHorizontal0 = anguloCamaraHorizontal;
						anguloCamaraVertical0 = anguloCamaraVertical;
						break;
					case osgGA::GUIEventAdapter::RIGHT_MOUSE_BUTTON:
						
						break;
				}
				break;
			}
			case(osgGA::GUIEventAdapter::RELEASE):
			{
				printf("release\n");
				switch (button)
				{
					case osgGA::GUIEventAdapter::LEFT_MOUSE_BUTTON:
						clickIzquierdo = false;
						//anguloCamaraHorizontal += (click.x - tecla_pulsada.x)/800*2;
						//anguloCamaraVertical += (click.y - tecla_pulsada.y)/800*2;
						break;
					case osgGA::GUIEventAdapter::RIGHT_MOUSE_BUTTON:

						break;
				}
				break;
				break;
			}
		}
		if (clickIzquierdo) {
			anguloCamaraHorizontal = anguloCamaraHorizontal0 + (click.x - tecla_pulsada.x) / 800 * 2;
			anguloCamaraVertical = anguloCamaraVertical0 + (click.y - tecla_pulsada.y) / 800 * 2;
		}

		str_coordMouse = "";
		str_coordMouse.append("Mouse: x=");
		str_coordMouse.append(std::to_string(int(tecla_pulsada.x)));
		str_coordMouse.append(", y=");
		str_coordMouse.append(std::to_string(int(tecla_pulsada.y)));
		str_coordMouse.append(" (pantalla:");
		str_coordMouse.append(std::to_string(int(windowLength)));
		str_coordMouse.append("x");
		str_coordMouse.append(std::to_string(int(windowHeight)));
		str_coordMouse.append(")");
		if (scroll_times == 0) {
			static unsigned int veces = 0;
			veces++;
			static double anguloCamaraHorizontal_1aPersona_Var = 0;
			static double anguloCamaraHorizontal_1aPersona_Mantener = 0;
			static double anguloCamaraVertical_1aPersona_Mantener = 0;
			double wl = windowLength;
			double wh = windowHeight;
			double tx = tecla_pulsada.x;
			double ty = tecla_pulsada.y;
			anguloCamaraHorizontal_1aPersona = (wl / 2.0 - tx) / 100.0 + anguloCamaraHorizontal_1aPersona_Mantener;
			anguloCamaraVertical_1aPersona = (wh / 2.0 - ty) / 100.0 + anguloCamaraVertical_1aPersona_Mantener;
			if (veces < 200) {
				Yo.setDireccion(Yo.getDireccion() + anguloCamaraHorizontal_1aPersona - anguloCamaraHorizontal_1aPersona_Var);
				anguloCamaraHorizontal_1aPersona_Var = anguloCamaraHorizontal_1aPersona;
			}
			else {
				veces = 0;
				Yo.setDireccion(Yo.getDireccion() + anguloCamaraHorizontal_1aPersona - anguloCamaraHorizontal_1aPersona_Var);
				anguloCamaraHorizontal_1aPersona_Mantener = anguloCamaraHorizontal_1aPersona;
				anguloCamaraVertical_1aPersona_Mantener = anguloCamaraVertical_1aPersona;
				//anguloCamaraHorizontal_1aPersona_Var = 0;
				viewer.requestWarpPointer(wl / 2.0, wh / 2.0);
			}
			//viewer.requestWarpPointer(windowLength / 2, windowHeight / 2);
		}

		return 0;
	}
};

int main(int, char**)
{

	GetDesktopResolution(screenLength, screenHeight);
	printf("pantalla:\n\tlength: %d\n\theight: %d\n", screenLength, screenHeight);
	windowLength = screenLength*(1 - 2 * margin);
	windowHeight = screenHeight*(1 - 2 * margin);

	viewer.setUpViewInWindow(screenLength*margin, screenHeight*margin, windowLength, windowHeight, 0); //estaba en: 800x600 
	setWindowTitle(viewer, "Juego del equipo ESOCIETY");//para poner un título en la ventana

	chunk_playit.setPath("Sonidos/play_it.wav");
	chunk_crows.setPath("Sonidos/Crows.wav");
	//chunk_crows.play(1);
	//pulsar 'm' para escuchar "play it"

	osg::Image* imagen_heightmap = osgDB::readImageFile(HEIGHTMAP_ARCHIVO);
	//osg::Image* imagen_heightmap = osgDB::readImageFile(".\\Data\\Texturas\\testIsland.png");
	

	//para las capturas de pantalla
	SnapImage* postDrawCallback = new SnapImage("Capturas/Captura");
	viewer.getCamera()->setPostDrawCallback(postDrawCallback);
	viewer.addEventHandler(new SnapeImageHandler('p', postDrawCallback));
	
	osg::Group * grupoEscenario = new osg::Group;
	osg::Group * grupoTotal = new osg::Group;

	calcularPosCamara();

	
	//----------------------------- Luces ----------------------------------
	std::string s1 = Sol.tellDayDuration();
	str_mostrar.append(s1);
	//Luz global
	osg::Geode* geodeSol = new osg::Geode();
	grupoEscenario->addChild(geodeSol);
	geodeSol->addDrawable(new osg::ShapeDrawable(new osg::Sphere(osg::Vec3(Sol.getX(), Sol.getY(), Sol.getZ()+5), 2)));//para ver de dónde viene la luz

	osg::Light* myLight = new osg::Light;
	myLight->setLightNum(0);
	myLight->setPosition(osg::Vec4(Sol.getX(), Sol.getY(), Sol.getZ(), 1.0f));		// Posicion foco
	myLight->setAmbient(osg::Vec4(1.0f,1.0f,1.0f,1.0f));							// Color luz ambiente (3 primeros valores son RGB, el último es para cantidad)
	myLight->setDiffuse(osg::Vec4(0.5f,0.5f,0.5f,1.0f));							// Color luz difusa
    //myLight->setDirection(osg::Vec3(1.0f,0.0f,-1.0f));							// Direccion
	//myLight->setSpotCutoff(15);													//ángulo del cono de luz (máximo 90 para cono de 180 grados)
	//myLight->setConstantAttenuation(0.1);											//atenúa la luz en función
	//myLight->setLinearAttenuation(0.1);											//atenúa la luz en función de la distancia
	//myLight->setQuadraticAttenuation(0.1);										//atenúa la luz en función del cuadrado de la distancia

	osg::LightSource* lightS = new osg::LightSource;
	lightS->setLight(myLight);
    lightS->setLocalStateSetModes(osg::StateAttribute::ON);

	osg::Group * grupoLuz = new osg::Group;
	grupoLuz->addChild(lightS);
	grupoTotal->addChild(grupoLuz);

	
	//------------------------------ Sombras ------------------------------------	
	
	osgShadow::ShadowedScene* ss = new  osgShadow::ShadowedScene;
	osg::ref_ptr<osgShadow::LightSpacePerspectiveShadowMapVB> sm = new osgShadow::LightSpacePerspectiveShadowMapVB;

	ss->setShadowTechnique(sm.get());

	float minLightMargin = 10.f;
	float maxFarPlane = 1000;
	unsigned int texSize = 1024;
	unsigned int baseTexUnit = 0;
	unsigned int shadowTexUnit = 6;

	texSize = 4096;
	sm->setLight(myLight);
	sm->setMinLightMargin( minLightMargin );
	sm->setMaxFarPlane( maxFarPlane );
 	sm->setTextureSize( osg::Vec2s( texSize, texSize ) );		
	sm->setShadowTextureCoordIndex( shadowTexUnit );
	sm->setShadowTextureUnit( shadowTexUnit );
	sm->setBaseTextureCoordIndex( baseTexUnit );
	sm->setBaseTextureUnit( baseTexUnit );

	const int ReceivesShadowTraversalMask = 0x1;
	const int CastsShadowTraversalMask = 0x2;

	ss->setReceivesShadowTraversalMask(ReceivesShadowTraversalMask);
    ss->setCastsShadowTraversalMask(CastsShadowTraversalMask);

	grupoTotal->addChild(ss);
	ss->addChild(grupoEscenario);

	//viewer.setSceneData(grupoTotal);
	
	//std::cout << "hola!!!";

	//--------------------------- Camara_texto ------------------------------- 
	osg::Camera* camera_texto = new osg::Camera;
	camera_texto->setProjectionMatrix(osg::Matrix::ortho2D(-400,400,-300,300));	
	camera_texto->setReferenceFrame(osg::Transform::ABSOLUTE_RF);			
	camera_texto->setViewMatrix(osg::Matrix::identity());
	camera_texto->setClearMask(GL_DEPTH_BUFFER_BIT);						
	camera_texto->setRenderOrder(osg::Camera::POST_RENDER);				
	grupoTotal->addChild(camera_texto);

	osgWidget::WindowManager* wm = new osgWidget::WindowManager(
		&viewer,
		double(windowLength),
		double(windowHeight),
		MASK_2D,
		osgWidget::WindowManager::WM_PICK_DEBUG
	);
	
	//osgWidget::Window* box = viewer.getW
	//box->addCallback?
	osgWidget::Window* menu = new osgWidget::Box("box", osgWidget::Box::VERTICAL);
	menu->setAnchorVertical(osgWidget::Window::VA_TOP);
	menu->setAnchorHorizontal(osgWidget::Window::HA_RIGHT);
	ColorLabelMenu* labelMenu = new ColorLabelMenu("Menu");
	//ColorLabelMenu* label2 = new ColorLabelMenu("Menu 2");
	menu->addWidget(labelMenu);
	menu->update();
	//menu->addWidget(label2);
	wm->addChild(menu);
	camera_texto->addChild(wm);
	menu->getBackground()->setColor(0.6f, 0.6f, 0.6f, 1.0f);
	menu->resize();
	menu->resizePercent(10.0);
	//osgWidget::createExample(viewer, wm, grupoEscenario);
	

	//para los diálogos
	osg::ref_ptr<osgWidget::Box> boxDialogo = new osgWidget::Box("BOXDIALOGOS", osgWidget::Box::VERTICAL);

	osg::ref_ptr<PreguntaLabel> pregunta = new PreguntaLabel("Frank", "Frase de Frank", windowLength*0.015, windowHeight*0.025);
	osg::ref_ptr<RespuestaLabel> respuesta1 = new RespuestaLabel(1, "Repuesta 1", windowLength*0.15, windowHeight*0.025);
	osg::ref_ptr<RespuestaLabel> respuesta2 = new RespuestaLabel(2, "Repuesta 2", windowLength*0.15, windowHeight*0.025);
	osg::ref_ptr<RespuestaLabel> respuesta3 = new RespuestaLabel(3, "Repuesta 3", windowLength*0.15, windowHeight*0.025);
	
	/*boxDialogo->addWidget(respuesta3);
	boxDialogo->addWidget(respuesta2);
	boxDialogo->addWidget(respuesta1);
	boxDialogo->addWidget(pregunta);*/
	/*boxDialogo->setAnchorVertical(osgWidget::Window::VA_TOP);*/
	boxDialogo->setAnchorHorizontal(osgWidget::Window::HA_LEFT);
	boxDialogo->getBackground()->setColor(COLOR_GRIS_OSCURO);

	//boxDialogo->setX(windowLength*0.75);
	boxDialogo->setY(windowHeight*0.55);
	//wm->addChild(boxDialogo);

	//diálogos de Jack
	rapidxml::xml_document<> docD;
	// Read the xml file into a vector
	osgDB::ifstream theFileD(".\\Archivos\\dialogosJack.xml");
	std::vector<char> bufferD((std::istreambuf_iterator<char>(theFileD)), std::istreambuf_iterator<char>());
	bufferD.push_back('\0');
	// Parse the buffer using the xml file parsing library into doc }
	docD.parse<0>(&bufferD[0]);
	rapidxml::xml_node<> *nodoD0;
	nodoD0 = docD.first_node();
	int i = 0;
	for (rapidxml::xml_node<>* nodoD1 = nodoD0->first_node("dialogo"); nodoD1; nodoD1 = nodoD1->next_sibling("dialogo")) {
		std::string personaje = nodoD1->first_node("personaje")->value();
		printf("%s\n", personaje);
		if (personaje == "Jack")
			i++;
	}
	numDialogosJack = i;
	printf("numDialogosJack: %d\n", numDialogosJack);
	Dialogo* dialogosJack = new Dialogo[numDialogosJack];
	i = 0;
	for (rapidxml::xml_node<>* nodoD1 = nodoD0->first_node("dialogo"); nodoD1; nodoD1 = nodoD1->next_sibling("dialogo")) {
		std::string fase = nodoD1->first_node("fase")->value();
		std::string personaje = nodoD1->first_node("personaje")->value();
		if(personaje=="Jack"){
			dialogosJack[i].fase = fase;
			dialogosJack[i].personaje = personaje;
			printf("%s\n",fase.c_str());
			printf("%s\n", personaje.c_str());

			rapidxml::xml_node<>* nodoP = nodoD1->first_node("pregunta");
			std::string pregunta = nodoP->value();
			std::string da = nodoP->first_attribute()->value();
			std::string conrespuesta = nodoP->first_attribute()->next_attribute()->value();
			dialogosJack[i].pregunta.pregunta = pregunta;
			dialogosJack[i].pregunta.da = da;
			dialogosJack[i].pregunta.conrespuesta = atoi(conrespuesta.c_str());
			printf("%s\n", pregunta.c_str());
			printf("%s\n", da.c_str());
			printf("%s\n", conrespuesta.c_str());
		
			rapidxml::xml_node<>* nodoR1;
			std::string respuesta, siguientefase, necesario, sino;
			rapidxml::xml_attribute<>* a;

			for (int j = 0; j < 3; j++) {
				std::string s = "respuesta" + std::to_string(j+1);
				rapidxml::xml_node<>* nodoR = nodoD1->first_node(s.c_str());
				respuesta = nodoR->value();
				siguientefase = nodoR->first_attribute()->value();
				necesario = nodoR->first_attribute()->next_attribute()->value();
				sino = nodoR->first_attribute()->next_attribute()->next_attribute()->value();
				dialogosJack[i].respuesta[j].respuesta = respuesta;
				dialogosJack[i].respuesta[j].siguientefase = siguientefase;
				dialogosJack[i].respuesta[j].necesario = necesario;
				dialogosJack[i].respuesta[j].sino = sino;
				printf("%s\n", respuesta.c_str());
				printf("%s\n", siguientefase.c_str());
				printf("%s\n", necesario.c_str());
				printf("%s\n", sino.c_str());
			}
			printf("\n");
			i++;
		}
	}


	//para el MAPA

	/*{
		double t1, t2;
		t1 = ::GetCurrentTime();
		CopyFile(HEIGHTMAP_ARCHIVO, MAPA_ARCHIVO, FALSE);
		t2 = ::GetCurrentTime();
		printf("tiempo en copiar el archivo: %lf ms", t2 - t1);
	}*/

	osg::Image* imagenMapa = osgDB::readImageFile(MAPA_ARCHIVO);
	//osg::Image* imagenMapaBYN = osgDB::readImageFile(MAPA_ARCHIVO);
	//osg::Image* imagenMapaFinal = osgDB::readImageFile(MAPA_ARCHIVO);
	//unsigned char* c = imagenMapaBYN->data();
	////printf("imageMapa:\n%s\n", c);
	//{
	//	int i = 0;
	//	while (c[i] != 3) {
	//		if(c[i]==0xFF)
	//			printf("%c", c[i]);
	//		else
	//			printf("%02X", int(c[i]));
	//		i++;
	//	}
	//	printf("\ncaracteres: %d\n", i - 1);
	//}
	////printf("%s\n", c);
	//system("pause");

	/*osgWidget::Window**/ boxMap = new osgWidget::Box("BOXMAP", osgWidget::Box::HORIZONTAL, true);
	/*osgWidget::Widget**/ widgetMap = new ColorWidget("WIDGETMAP", 256.0f, 256.0f);
	widgetMap->setImage(imagenMapa);
	widgetMap->setTexCoord(0.0f, 0.0f, osgWidget::Widget::LOWER_LEFT);
	widgetMap->setTexCoord(1.0f, 0.0f, osgWidget::Widget::LOWER_RIGHT);
	widgetMap->setTexCoord(1.0f, 1.0f, osgWidget::Widget::UPPER_RIGHT);
	widgetMap->setTexCoord(0.0f, 1.0f, osgWidget::Widget::UPPER_LEFT);
	widgetMap->setSize(200,200);
	boxMap->addWidget(widgetMap);
	wm->addChild(boxMap);

	//----------------------------- Texto ----------------------------
	osg::ref_ptr<osgText::Font> font = osgText::readRefFontFile("./Otros/fonts/arial.ttf");
	osg::Geode* geode_texto_intro = new osg::Geode();
	osgText::Text* texto_intro = new osgText::Text;
	geode_texto_intro->addDrawable(texto_intro);
	camera_texto->addChild(geode_texto_intro);
	texto_intro->setFont(font);
	texto_intro->setPosition(osg::Vec3(0, windowHeight*0.9, 0));
	texto_intro->setColor(osg::Vec4(255, 255, 255, 255));
	texto_intro->setFontResolution(40,40);
	texto_intro->setCharacterSizeMode(osgText::Text::SCREEN_COORDS);
	texto_intro->setCharacterSize(20);
	texto_intro->setAlignment(osgText::Text::LEFT_BOTTOM);
	texto_intro->setText(str_mostrar);

	//mostrar coordenadas del personaje
	osg::Geode* geode_texto_intro2 = new osg::Geode();
	osgText::Text* texto_intro2 = new osgText::Text;
	geode_texto_intro2->addDrawable(texto_intro2);
	camera_texto->addChild(geode_texto_intro2);
	texto_intro2->setFont(font);
	texto_intro2->setPosition(osg::Vec3(0, windowHeight*0.92, 0));
	texto_intro2->setColor(osg::Vec4(255, 255, 255, 255));
	texto_intro2->setFontResolution(40, 40);
	texto_intro2->setCharacterSizeMode(osgText::Text::SCREEN_COORDS);
	texto_intro2->setCharacterSize(20);
	texto_intro2->setAlignment(osgText::Text::LEFT_BOTTOM);
	texto_intro2->setText("coordenadas del personaje");

	//mostrar posición del ratón
	osg::Geode* geode_texto_intro3 = new osg::Geode();
	osgText::Text* texto_intro3 = new osgText::Text;
	geode_texto_intro3->addDrawable(texto_intro3);
	camera_texto->addChild(geode_texto_intro3);
	texto_intro3->setFont(font);
	texto_intro3->setPosition(osg::Vec3(-400, -250, 0));
	texto_intro3->setColor(osg::Vec4(255, 255, 255, 255));
	texto_intro3->setFontResolution(40, 40);
	texto_intro3->setCharacterSizeMode(osgText::Text::SCREEN_COORDS);
	texto_intro3->setCharacterSize(20);
	texto_intro3->setAlignment(osgText::Text::LEFT_BOTTOM);
	texto_intro3->setText("posición del ratón");

	//toast
	osg::Geode* geode_toast = new osg::Geode();
	osgText::Text* texto_toast = new osgText::Text;
	geode_toast->addDrawable(texto_toast);
	camera_texto->addChild(geode_toast);
	texto_toast->setFont(font);
	texto_toast->setPosition(osg::Vec3(windowLength/2, 10, 0));
	texto_toast->setColor(osg::Vec4(255, 255, 255, 255));
	texto_toast->setFontResolution(40, 40);
	texto_toast->setCharacterSizeMode(osgText::Text::SCREEN_COORDS);
	texto_toast->setCharacterSize(20);
	texto_toast->setAlignment(osgText::Text::CENTER_BOTTOM);
	texto_toast->setText("toast");

	//toast
	osg::Geode* geode_tiempo = new osg::Geode();
	osgText::Text* texto_tiempo = new osgText::Text;
	geode_tiempo->addDrawable(texto_tiempo);
	camera_texto->addChild(geode_tiempo);
	texto_tiempo->setFont(font);
	texto_tiempo->setPosition(osg::Vec3(windowLength/2, windowHeight*0.9, 0));
	texto_tiempo->setColor(osg::Vec4(255, 255, 255, 255));
	texto_tiempo->setFontResolution(40, 40);
	texto_tiempo->setCharacterSizeMode(osgText::Text::SCREEN_COORDS);
	texto_tiempo->setCharacterSize(50);
	texto_tiempo->setAlignment(osgText::Text::CENTER_BOTTOM);
	texto_tiempo->setText(str_tiempoRestante);

	/*osg::Geode* geode = new osg::Geode();
	osgText::Text* texto_intro4 = new osgText::Text;
	geode->addDrawable(texto_intro4);
	//botton->addDescription(new osg::ShapeDrawable(new osg::)
	camera_texto->addChild(geode);
	texto_intro4->setFont(font);
	texto_intro4->setPosition(osg::Vec3(0, 0, 0));
	texto_intro4->setColor(osg::Vec4(255, 255, 255, 255));
	texto_intro4->setFontResolution(80, 80);
	texto_intro4->setCharacterSizeMode(osgText::Text::SCREEN_COORDS);
	texto_intro4->setCharacterSize(20);
	texto_intro4->setAlignment(osgText::Text::LEFT_BOTTOM);
	texto_intro4->setText("posición del ratón");*/

	/*osg::BoundingBox bb;
	for (unsigned int i = 0; i<geode->getNumDrawables(); ++i)
	{
		bb.expandBy(geode->getDrawable(i)->getBound());
	}
	osg::Geometry* geom = new osg::Geometry;

	osg::Vec3Array* vertices = new osg::Vec3Array;
	float depth = bb.zMin() - 0.1;
	vertices->push_back(osg::Vec3(bb.xMin(), bb.yMax(), depth));
	vertices->push_back(osg::Vec3(bb.xMin(), bb.yMin(), depth));
	vertices->push_back(osg::Vec3(bb.xMax(), bb.yMin(), depth));
	vertices->push_back(osg::Vec3(bb.xMax(), bb.yMax(), depth));
	geom->setVertexArray(vertices);*/

	/*osg::Vec3Array* normals = new osg::Vec3Array;
	normals->push_back(osg::Vec3(0.0f, 0.0f, 1.0f));
	geom->setNormalArray(normals);
	geom->setNormalBinding(osg::Geometry::BIND_OVERALL);*/

	/*osg::ref_ptr<osg::Vec4Array> c = new osg::Vec4Array;		//la línea con Vec4Array da error: símbolo externo sin resolver (¿falta algun .lib en el input linker?)
	geom->setColorArray(c.get());
	geom->setColorBinding(osg::Geometry::BIND_PER_VERTEX);
	c->push_back(osg::Vec4(1.0f, 0.0f, 0.0f, 1.0f));
	c->push_back(osg::Vec4(0.0f, 1.0f, 0.0f, 1.0f));
	c->push_back(osg::Vec4(0.0f, 0.0f, 1.0f, 1.0f));
	c->push_back(osg::Vec4(1.0f, 1.0f, 1.0f, 1.0f));*/
	/*osg::ref_ptr<osg::Vec4Array> c = new osg::Vec4Array;
	colors->push_back(osg::Vec4(1.0f, 1.0, 0.8f, 0.2f));
	geom->setColorArray(colors);
	geom->setColorBinding(osg::Geometry::BIND_OVERALL);*/

	/*geom->addPrimitiveSet(new osg::DrawArrays(GL_QUADS, 0, 4));*/

	/*osg::Texture2D* tex = new osg::Texture2D;
	osg::Image* image = osgDB::readImageFile(".\\Data\\Texturas\\Heightmap_rgb.png");
	tex->setImage(image);
	osg::StateSet* state = geom->getOrCreateStateSet();
	//state->setMode(GL_BLEND, osg::StateAttribute::ON);
	//state->setRenderingHint(osg::StateSet::TRANSPARENT_BIN);
	state->setTextureAttributeAndModes(0, tex, osg::StateAttribute::ON);
	geode->setStateSet(state);*/
	
	/*geode->addDrawable(geom);*/


	/*osg::ref_ptr<osg::Geometry> geom = new osg::Geometry;
	osg::ref_ptr<osg::Vec3Array> v = new osg::Vec3Array;
	geom->setVertexArray(v.get());
	double l = 6;
	v->push_back(osg::Vec3(-l, 0, -l));
	v->push_back(osg::Vec3(l, 0, -l));
	v->push_back(osg::Vec3(l, 0, l));
	v->push_back(osg::Vec3(-l, 0, l));
	osg::ref_ptr<osg::Vec4Array> c = new osg::Vec4Array;*/
	/*geom->setColorArray(c.get());
	geom->setColorBinding(osg::Geometry::BIND_PER_VERTEX);
	c->push_back(osg::Vec4(1.0f, 0.0f, 0.0f, 1.0f));
	c->push_back(osg::Vec4(0.0f, 1.0f, 0.0f, 1.0f));
	c->push_back(osg::Vec4(0.0f, 0.0f, 1.0f, 1.0f));
	c->push_back(osg::Vec4(1.0f, 1.0f, 1.0f, 1.0f));*/
	
	/*osg::ref_ptr<osg::Vec3Array> n = new osg::Vec3Array;
	geom->setNormalArray(n.get());
	geom->setNormalBinding(osg::Geometry::BIND_OVERALL);
	n->push_back(osg::Vec3(0.0f, -1.0f, 0.0f));*/
	/*geom->addPrimitiveSet(new osg::DrawArrays(osg::PrimitiveSet::QUADS, 0, 4)); 
	osg::ref_ptr<osg::Geode> newGeode = new osg::Geode;
	newGeode->addDrawable(geom.get());

	osg::Texture2D* tex = new osg::Texture2D;
	osg::Image* image = osgDB::readImageFile(".\\Data\\Texturas\\Heightmap_rgb.png");
	tex->setImage(image);
	osg::StateSet* stateOne = new osg::StateSet();
	stateOne->setTextureAttributeAndModes(0, tex, osg::StateAttribute::ON);
	newGeode->setStateSet(stateOne);
	osg::MatrixTransform* mtNew = new osg::MatrixTransform;
	mtNew->addChild(newGeode);
	grupoEscenario->addChild(mtNew);
	osg::Matrixd mtrasP, mrotP, mrotzP, mrotxP, mrotyP, mescP, mtotalP;
	osg::Vec3d pos2(3, -33, 1);
	mtrasP = mtrasP.translate(pos2);
	mrotzP = mrotzP.rotate(0, osg::Vec3d(0, 0, 1));
	mescP = mtrasP.scale(0.1, 0.1, 0.1);
	mtotalP = mescP*mrotP*mtrasP;
	mtNew->setMatrix(mtotalP);*/


	

	//-----------Codigo editado---------//
	//std::cout << "hola";
	printf("bienvenido\n");

	/*std::string geometria1 = "Tree.obj";
	std::string geometria2 = "Palma 001.obj";
	std::string geometria3 = "FinalBaseMesh.obj";
	osg::Node* nodo = osgDB::readNodeFile(".\\Texturas\\Trees\\" + geometria1);//lee en la carpeta tree y le añade la geometria 
	osg::Node* nodo2 = osgDB::readNodeFile(".\\Texturas\\Trees\\"+ geometria2);
	osg::Node* nodoH = osgDB::readNodeFile(".\\Texturas\\Humanos\\"+ geometria3);
	//grupoEscenario->addChild(nodo); 
	
	osg::MatrixTransform* mt = new osg::MatrixTransform;
	mt->addChild(nodo);
	grupoEscenario->addChild(mt);
	osg::Matrixd mtras, mrot, mrotz, mrotx, mroty, mesc, mtotal, mtras2;
	osg::Vec3d pos(10, 0, 0);
	mtras = mtras.translate(pos);
	mrotz = mrotz.rotate(0, osg::Vec3d(0, 0, 1));
	mesc = mtras.scale(0.1, 0.1, 0.1);
	mtotal = mesc*mrot*mtras;
	mt->setMatrix(mtotal);

	osg::MatrixTransform* mt2 = new osg::MatrixTransform;
	mt2->addChild(nodo);
	grupoEscenario->addChild(mt2);
	//osg::Matrixd mtras, mrot, mrotz, mrotx, mroty, mesc, mtotal, mtras2;
	osg::Vec3d pos2(-10, 0, 0);
	mtras = mtras.translate(pos2);
	mrotz = mrotz.rotate(0, osg::Vec3d(0, 0, 1));
	mesc = mtras.scale(0.12, 0.12, 0.12);
	mtotal = mesc*mrot*mtras;
	mt2->setMatrix(mtotal);

	osg::MatrixTransform* mt3 = new osg::MatrixTransform;
	mt3->addChild(nodo2);
	grupoEscenario->addChild(mt3);
	//osg::Matrixd mtras, mrot, mrotz, mrotx, mroty, mesc, mtotal, mtras2;
	osg::Vec3d pos3(0, -5, 0);
	mtras = mtras.translate(pos3);
	mrotz = mrotz.rotate(0, osg::Vec3d(0, 0, 1));
	mesc = mtras.scale(0.1, 0.1, 0.1);
	mtotal = mesc*mrot*mtras;
	mt3->setMatrix(mtotal);*/

	/*****xml inicio****/

	
	//añadimos heightmap
	osg::Geode* malla = CreaMalla(NUM_PUNTOS, SEPARACION_PUNTOS);											// Crea la malla del suelo
	AddTexture(malla, HEIGHTMAP_ARCHIVO, 0);			// Añade la textura a la malla (heightmap)
	AddTexture(malla, HEIGHTMAP_ARCHIVO_NORMALES, 1);		// Añade la textura a la malla (normales)
	AddTexture(malla, HEIGHTMAP_ARCHIVO_TEXTURAS, 2);		// Añade la textura a la malla (texturas)
	AddTexture(malla, ".\\Data\\Texturas\\cesped.dds", 3);			// Añade la textura a la malla (textura1)
	AddTexture(malla, ".\\Data\\Texturas\\snow.dds", 4);			// Añade la textura a la malla (textura2)
	AddTexture(malla, ".\\Data\\Texturas\\mountain.dds", 5);		// Añade la textura a la malla (textura3)
	AddShader(malla, ".\\Data\\heightmap_sombras.vert", ".\\Data\\heightmap_sombras.frag");

	osg::MatrixTransform* mtM = new osg::MatrixTransform;
	mtM->addChild(malla);
	grupoEscenario->addChild(mtM);
	osg::Matrixd mtras5, mrot5, mrotz5, mrotx5, mroty5, mesc5, mtotal5;
	osg::Vec3d pos(0, 0, 0);
	mtras5 = mtras5.translate(pos);
	mrotz5 = mrotz5.rotate(0, osg::Vec3d(0, 0, 1));
	mesc5 = mtras5.scale(1, 1, 1);
	mtotal5 = mesc5*mrot5*mtras5;
	mtM->setMatrix(mtotal5);
	//grupoEscenario->addChild(mt);


	rapidxml::xml_document<> doc;
	// Read the xml file into a vector
	osgDB::ifstream theFile(".\\Archivos\\trees.xml");
	std::vector<char> buffer((std::istreambuf_iterator<char>(theFile)), std::istreambuf_iterator<char>());
	buffer.push_back('\0');
	// Parse the buffer using the xml file parsing library into doc 
	doc.parse<0>(&buffer[0]);
	
	rapidxml::xml_node<> *nodo0;
	nodo0 = doc.first_node();
		

	//contamos árboles del archivo trees.xml
	/*int*/ i = 0;
	for (rapidxml::xml_node<>* nodo1 = nodo0->first_node("tree"); nodo1; nodo1 = nodo1->next_sibling("tree")) {
		i++;
	}
	printf("numero de arboles: %d\n", i);

	//contamos geometrías del archivo trees.xml y creamos vectores para guardar sus nombres y sus valores de escalado
	i = 0;
	for (rapidxml::xml_node<>* nodo1 = nodo0->first_node("geometria"); nodo1; nodo1 = nodo1->next_sibling("geometria")) {
		i++;
	}
	printf("numero de geometrias: %d\n", i);
	std::string* strG = new std::string[i];
	double *Escala = new double[i];
	
	//guardamos los valores de las geometrías en dichos vectores
	i = 0;
	for (rapidxml::xml_node<>* nodo1 = nodo0->first_node("geometria"); nodo1; nodo1 = nodo1->next_sibling("geometria")) {
		strG[i] = nodo1->value();
		std::string strE= nodo1->first_attribute()->value();
		Escala[i] = atof(strE.c_str());
		i++;
	}

	//creamos los árboles
	i = 0;
	for (rapidxml::xml_node<>* nodo1 = nodo0->first_node("tree"); nodo1; nodo1 = nodo1->next_sibling("tree")) {
		
		printf("\tCargando arbol %d (", i+1);
		int j = 0;
		char *c = nodo1->name();
		while (c[j] != '\0') { printf("%c", c[j]); j++; }
		printf(")\n");
		i++;

		//ponemos los árboles en sus posiciones
		std::string str = nodo1->first_node("fichero")->value();
		int g = atoi(str.c_str()); //número de la geometria
		std::string strX= nodo1->first_node("x")->value();
		double x = atof(strX.c_str());//pasa el string a un valor tipo double para usarlo
		std::string strY= nodo1->first_node("y")->value();
		double y = atof(strY.c_str()); 
		//std::string strZ= nodo1->first_node("z")->value();
		//double z = atof(strZ.c_str());
		double z = obtenerAltura(x, y, imagen_heightmap, NUM_PUNTOS, SEPARACION_PUNTOS, HEIGHTMAP_ALTURA);
		osg::Node* nodoG = osgDB::readNodeFile(".\\Texturas\\Trees\\" + strG[g-1]);//añade el nombre a partir del número de la geometría
		osg::MatrixTransform* mt = new osg::MatrixTransform;
	
		mt->addChild(nodoG);
		grupoEscenario->addChild(mt);
		osg::Matrixd mtras, mrot, mrotz, mrotx, mroty, mesc, mtotal, mtras2;
		osg::Vec3d pos(x, y, z);
		mtras = mtras.translate(pos);
		mrotz = mrotz.rotate(0, osg::Vec3d(0, 0, 1));
		double e = Escala[g - 1];
		mesc = mtras.scale(e,e,e);
		mtotal = mesc*mrot*mtras;
		mt->setMatrix(mtotal);

		//quién da sombra
		mt->setNodeMask(CastsShadowTraversalMask);

	}
	

	/*****xml fin******/
	
	osg::Node* nodoH = osgDB::readNodeFile(".\\Texturas\\Humanos\\FinalBaseMesh.obj");
	//osg::Node* nodoH = osgDB::readNodeFile(".\\Texturas\\Humanos\\biped1.osg");
	osg::MatrixTransform* mtH = new osg::MatrixTransform;
	mtH->addChild(nodoH);
	ss->addChild(mtH);
	/*las dos siguientes líneas harían que mtH y sus hijos no se vieran*/
	//mtH->setNodeMask( 0x2 ); //only 2nd bit set
	//viewer.getCamera()->setCullMask( 0x1 ); //2nd bit not set
	osg::Matrixd mtras, mrot, mrotz, mrotx, mroty, mesc, mtotal;
	Yo.setZ(obtenerAltura(Yo.getX(), Yo.getY(), imagen_heightmap, NUM_PUNTOS, SEPARACION_PUNTOS, HEIGHTMAP_ALTURA));
	osg::Vec3d posH(Yo.getX(), Yo.getY(), Yo.getZ());
	mtras = mtras.translate(posH);
	mrotz = mrotz.rotate(Yo.getDireccion()+PI/2, osg::Vec3d(0, 0, 1));
	mesc = mtras.scale(.1, .1, .1);
	mtotal = mesc*mrot*mtras;
	mtH->setMatrix(mtotal);

	osg::Node* nodoJack = osgDB::readNodeFile(".\\Texturas\\Humanos\\John_Mathew_LP_Texturized.obj");
	//osg::Node* nodoH = osgDB::readNodeFile(".\\Texturas\\Humanos\\biped1.osg");
	osg::MatrixTransform* mtJack = new osg::MatrixTransform;
	mtJack->addChild(nodoH);
	ss->addChild(mtJack);
	/*las dos siguientes líneas harían que mtJack y sus hijos no se vieran*/
	//mtJack->setNodeMask( 0x2 ); //only 2nd bit set
	//viewer.getCamera()->setCullMask( 0x1 ); //2nd bit not set
	osg::Matrixd mtrasJ, mrotJ, mrotzJ, mrotxJ, mrotyJ, mescJ, mtotalJ;
	Jack.setZ(obtenerAltura(Jack.getX(), Jack.getY(), imagen_heightmap, NUM_PUNTOS, SEPARACION_PUNTOS, HEIGHTMAP_ALTURA));
	osg::Vec3d posJack(Jack.getX(), Jack.getY(), Jack.getZ());
	mtrasJ = mtrasJ.translate(posJack);
	mrotzJ = mrotzJ.rotate(Jack.getDireccion() + PI / 2, osg::Vec3d(0, 0, 1));
	mescJ = mtrasJ.scale(.1, .1, .1);
	mtotalJ = mescJ*mrotJ*mtrasJ;
	mtJack->setMatrix(mtotalJ);
	
	osg::Node* nodoB = osgDB::readNodeFile(".\\Texturas\\Edificios\\Farmhouse OBJ.obj");
	osg::MatrixTransform* mtB = new osg::MatrixTransform;
	mtB->addChild(nodoB);
	grupoEscenario->addChild(mtB);
	osg::Matrixd mtras2, mrot2, mrotz2, mrotx2, mroty2, mesc2;
	double xB = -100, yB = -80;
	double zB = obtenerAltura(xB, yB, imagen_heightmap, NUM_PUNTOS, SEPARACION_PUNTOS, HEIGHTMAP_ALTURA);
	osg::Vec3d posB(xB,yB,zB);
	mtras = mtras.translate(posB);
	mrotz = mrotz.rotate(0, osg::Vec3d(0, 0, 1));
	mesc = mtras.scale(0.5, 0.5, 0.5);
	mtotal = mesc*mrot*mtras;
	mtB->setMatrix(mtotal);
	
	osg::Geode* geodeSky = new osg::Geode();
	osg::Image *imageSky = osgDB::readImageFile("./Texturas/Cielo/Sky_horiz_6-2048.jpg");
	//osg::Image *imageSky = osgDB::readImageFile("./Texturas/Cielo/skydome2-1024x1024.jpg");
	if (!imageSky)
		printf("\tImposible cargar textura cielo\n");
	osg::Texture2D *texture = new osg::Texture2D;
		texture->setDataVariance(osg::Object::DYNAMIC);
		texture->setFilter(osg::Texture::MIN_FILTER, osg::Texture::LINEAR);
		texture->setFilter(osg::Texture::MAG_FILTER, osg::Texture::LINEAR);
		texture->setWrap(osg::Texture::WRAP_S, osg::Texture::CLAMP_TO_EDGE);
		texture->setWrap(osg::Texture::WRAP_T, osg::Texture::CLAMP_TO_EDGE);
		texture->setWrap(osg::Texture::WRAP_R, osg::Texture::CLAMP_TO_EDGE);
		texture->setImage(imageSky);
	osg::StateSet *state = geodeSky->getOrCreateStateSet();
	state->ref();
	osg::Material *material = new osg::Material();
		material->setEmission(osg::Material::FRONT, osg::Vec4(1.0, 1.0, 1.0, 1.0));
		material->setAmbient(osg::Material::FRONT, osg::Vec4(1.0, 1.0, 1.0, 1.0));
		material->setShininess(osg::Material::FRONT, 25.0);
	state->setAttribute(material);
	state->setTextureAttributeAndModes(0, texture, osg::StateAttribute::ON);
	texture->setImage(imageSky);
	
	geodeSky->addDrawable(new osg::ShapeDrawable(new osg::Sphere(osg::Vec3(0, 0, 0), 1000)));

	osg::MatrixTransform* mtSky = new osg::MatrixTransform;
	grupoTotal->addChild(mtSky);
	mtSky->addChild(geodeSky);
	osg::Matrixd mtrasSky, mrotSky, mescSky, mtotalSky;
	mtrasSky = mtrasSky.translate(osg::Vec3d(0, 0, 0));
	mrotSky = mrotSky.rotate(-100, osg::Vec3d(-1, -1, 0));
	mescSky = mtrasSky.scale(1, 1, 1);
	mtotalSky = mescSky*mrotSky*mtrasSky;
	mtSky->setMatrix(mtotalSky);
	
	osg::Geode* geodeSpheres = new osg::Geode();
	grupoTotal->addChild(geodeSpheres);

	menu->resize();
	menu->resizePercent(10.0);

	//quién da sombra
	mtB->setNodeMask(CastsShadowTraversalMask);
	//mtH->setNodeMask(CastsShadowTraversalMask);
	//mt de los árboles (ya está puesto en el código cuando se crean los árboles)

	//quién recibe sombra
	mtJack->setNodeMask(ReceivesShadowTraversalMask);
	mtH->setNodeMask(ReceivesShadowTraversalMask);
	mtM->setNodeMask(ReceivesShadowTraversalMask);

	
	//////////////////////////////////////preparar MENU_INICIO///////////////////////////////////////////////////////////////////////////////////
	osgWidget::WindowManager* wmMenu = new osgWidget::WindowManager(
		&viewer,
		double(windowLength),
		double(windowHeight),
		MASK_2D,
		// osgWidget::WindowManager::WM_USE_RENDERBINS |
		osgWidget::WindowManager::WM_PICK_DEBUG
	);
	osgWidget::Box* boxMenu = new osgWidget::Box("BOXMENU", osgWidget::Box::VERTICAL);

	MenuLabel* label1 = new MenuLabel(MENU_OPCION1);
	MenuLabel* label2 = new MenuLabel(MENU_OPCION2);
	MenuLabel* label3 = new MenuLabel(MENU_OPCION3);
	MenuLabel* label4 = new MenuLabel(MENU_OPCION4);
	MenuLabel* label5 = new MenuLabel(MENU_OPCION5);

	boxMenu->addWidget(label5);
	boxMenu->addWidget(label4);
	boxMenu->addWidget(label3);
	boxMenu->addWidget(label2);
	boxMenu->addWidget(label1);
	boxMenu->setAnchorVertical(osgWidget::Window::VA_TOP);
	boxMenu->setAnchorHorizontal(osgWidget::Window::HA_CENTER);
	boxMenu->getBackground()->setColor(COLOR_ROJO_CLARITO);

	boxMenu->setY(windowHeight / 2);
	wmMenu->addChild(boxMenu);

	//osg::Camera* camera_menu = new osg::Camera;
	osg::Camera* camera_menu = wmMenu->createParentOrthoCamera();
	camera_menu->setProjectionMatrix(osg::Matrix::ortho2D(-400, 400, -300, 300));
	camera_menu->setReferenceFrame(osg::Transform::ABSOLUTE_RF);
	camera_menu->setViewMatrix(osg::Matrix::identity());
	camera_menu->setClearMask(GL_DEPTH_BUFFER_BIT);
	camera_menu->setRenderOrder(osg::Camera::POST_RENDER);
	//camera_menu->addChild(wmMenu);
	osg::Group * grupoMenu = new osg::Group;
	grupoMenu->addChild(camera_menu);

	osg::Geode* geode_texto_menu = new osg::Geode();
	osgText::Text* texto_menu = new osgText::Text;
	geode_texto_menu->addDrawable(texto_menu);
	camera_menu->addChild(geode_texto_menu);
	texto_menu->setFont(font);
	texto_menu->setPosition(osg::Vec3(300, 0, 0));
	texto_menu->setColor(osg::Vec4(255, 255, 255, 255));
	texto_menu->setFontResolution(40, 40);
	texto_menu->setCharacterSizeMode(osgText::Text::SCREEN_COORDS);
	texto_menu->setCharacterSize(20);
	texto_menu->setAlignment(osgText::Text::LEFT_BOTTOM);
	texto_menu->setText(str_mostrar);

	//viewer.addEventHandler(new osgWidget::MouseHandler(wmMenu));
	////viewer.addEventHandler(new osgWidget::KeyboardHandler(wm));
	//viewer.addEventHandler(new osgWidget::ResizeHandler(wmMenu, camera_menu));
	////viewer.addEventHandler(new osgWidget::CameraSwitchHandler(wm, camera_texto));
	////viewer.addEventHandler(new osgViewer::StatsHandler());
	////viewer.addEventHandler(new osgViewer::WindowSizeHandler());
	////viewer.addEventHandler(new osgGA::StateSetManipulator(
	////	viewer.getCamera()->getOrCreateStateSet()
	////));


	//////////////////////////////////////preparar INTRODUCIR_NOMBRE///////////////////////////////////////////////////////////////////////////////////
	osgWidget::WindowManager* wmNombre = new osgWidget::WindowManager(
		&viewer,
		double(windowLength),
		double(windowHeight),
		MASK_2D,
		// osgWidget::WindowManager::WM_USE_RENDERBINS |
		osgWidget::WindowManager::WM_PICK_DEBUG
	);
	osgWidget::Box* boxNombre = new osgWidget::Box("BOXNOMBRE", osgWidget::Box::VERTICAL);
	osgWidget::Input* inputNombre = new osgWidget::Input("INPUTNOMBRE", "", 100);

	inputNombre->setFont("./Otros/fonts/VeraMono.ttf");
	inputNombre->setFontColor(0.0f, 0.0f, 0.0f, 1.0f);
	inputNombre->setFontSize(20);
	inputNombre->setYOffset(inputNombre->calculateBestYOffset("y"));
	inputNombre->setSize(400.0f, inputNombre->getText()->getCharacterHeight());

	boxNombre->addWidget(inputNombre);
	boxNombre->setOrigin(double(windowLength)/2-inputNombre->getSize().length()/2, double(windowHeight)/2);
	
	wmNombre->addChild(boxNombre);

	//osg::Camera* camera_menu = new osg::Camera;
	osg::Camera* camera_nombre = wmNombre->createParentOrthoCamera();
	camera_nombre->setProjectionMatrix(osg::Matrix::ortho2D(-400, 400, -300, 300));
	camera_nombre->setReferenceFrame(osg::Transform::ABSOLUTE_RF);
	camera_nombre->setViewMatrix(osg::Matrix::identity());
	camera_nombre->setClearMask(GL_DEPTH_BUFFER_BIT);
	camera_nombre->setRenderOrder(osg::Camera::POST_RENDER);
	//camera_menu->addChild(wmMenu);
	osg::Group * grupoNombre = new osg::Group;
	grupoNombre->addChild(camera_nombre);

	osg::Geode* geode_texto_nombre = new osg::Geode();
	osgText::Text* texto_nombre = new osgText::Text;
	geode_texto_nombre->addDrawable(texto_nombre);
	camera_nombre->addChild(geode_texto_nombre);
	texto_nombre->setFont(font);
	texto_nombre->setPosition(osg::Vec3(double(windowLength)/2-inputNombre->getSize().length()/2, double(windowHeight)/2+inputNombre->getSize().y()*2, 0));
	texto_nombre->setColor(osg::Vec4(255, 255, 255, 255));
	texto_nombre->setFontResolution(40, 40);
	texto_nombre->setCharacterSizeMode(osgText::Text::SCREEN_COORDS);
	texto_nombre->setCharacterSize(20);
	texto_nombre->setAlignment(osgText::Text::LEFT_BOTTOM);
	texto_nombre->setText("Por favor, introduzce tu nombre y presiona enter");

	camera_nombre->addChild(geode_toast);
	
	/*viewer.addEventHandler(new osgWidget::MouseHandler(wmNombre));
	viewer.addEventHandler(new osgWidget::KeyboardHandler(wmNombre));
	viewer.addEventHandler(new osgWidget::ResizeHandler(wmNombre, camera_nombre));
	viewer.addEventHandler(new osgWidget::CameraSwitchHandler(wmNombre, camera_nombre));
	viewer.addEventHandler(new osgViewer::WindowSizeHandler());*/

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	while(!viewer.done()){

			switch (FASE_GENERAL) {

				case MENU_INICIO:
				{
						osgWidget::MouseHandler* menuMouseHandler = new osgWidget::MouseHandler(wmMenu);
						osgWidget::ResizeHandler* menuResizeHandler = new osgWidget::ResizeHandler(wmMenu, camera_menu);
						viewer.addEventHandler(menuMouseHandler);
						viewer.addEventHandler(menuResizeHandler);
						boxMenu->resize();
						wmMenu->resizeAllWindows();
						viewer.setSceneData(grupoMenu);
						viewer.realize();
						while (FASE_GENERAL == MENU_INICIO && !viewer.done()) {
							if (label1->clicado)
								FASE_GENERAL = INSERTAR_NOMBRE;
							else if (label2->clicado)
								FASE_GENERAL = MENU_INICIO;
							else if (label3->clicado)
								FASE_GENERAL = MENU_INICIO;
							else if (label4->clicado)
								FASE_GENERAL = MENU_INICIO;
							else if (label5->clicado)
								FASE_GENERAL = SALIR;

							Sleep(33);
							viewer.frame();
						}
						//viewer.setDone(false);
						viewer.removeEventHandler(menuResizeHandler);
						viewer.removeEventHandler(menuMouseHandler);
						break;
				}
				case INSERTAR_NOMBRE:
				{
						osgWidget::ResizeHandler* nombreResizeHandler = new osgWidget::ResizeHandler(wmNombre, camera_nombre);
						osgWidget::MouseHandler* nombreMouseHandler = new osgWidget::MouseHandler(wmNombre);
						osgWidget::KeyboardHandler* nombreKeyboardHandler = new osgWidget::KeyboardHandler(wmNombre);
						viewer.addEventHandler(nombreResizeHandler);
						viewer.addEventHandler(nombreMouseHandler);
						viewer.addEventHandler(nombreKeyboardHandler);


						NameInsertedHandler* nameInsertedHandler = new NameInsertedHandler(65293, inputNombre, NOMBRE);
						viewer.addEventHandler(nameInsertedHandler);

						wmNombre->resizeAllWindows();
						viewer.setSceneData(grupoNombre);
						viewer.realize();
						while (FASE_GENERAL == INSERTAR_NOMBRE && !viewer.done()) {
							if (NOMBRE.length() > 1) {
								static bool a = false;
								if (a == false) {
									std::string saludo = "Bienvenido " + NOMBRE + "\n";
									toast_global.addMessage(saludo, 3);
									a = true;
									FASE_GENERAL = JUGAR;
								}
							}
							texto_toast->setText(toast_global.getMessages());
							Sleep(33);
							viewer.frame();
						}
						viewer.removeEventHandler(nombreResizeHandler);
						viewer.removeEventHandler(nombreMouseHandler);
						viewer.removeEventHandler(nombreKeyboardHandler);
						viewer.removeEventHandler(nameInsertedHandler);
						break;
				}
				case JUGAR:
				{
						SimulacionKeyboardEvent* KeyboardEvent = new SimulacionKeyboardEvent();
						osgWidget::MouseHandler* mouseHandler = new osgWidget::MouseHandler(wm);
						osgWidget::ResizeHandler* resizeHandler = new osgWidget::ResizeHandler(wm, camera_texto);
						viewer.addEventHandler(KeyboardEvent); 
						viewer.addEventHandler(mouseHandler);
						viewer.addEventHandler(resizeHandler);
						
						labelMenu->reiniciarClick();

						double t1 = 0, t0 = 0;
						viewer.setSceneData(grupoTotal);
						viewer.realize();
						t1 = ::GetCurrentTime();//sino se pone esta línea, el primer t1-t0 es muy grande
						while (FASE_GENERAL == JUGAR && !viewer.done()) {

							t0 = t1;
							t1 = ::GetCurrentTime();
							double cycleTime = (t1 - t0) / 1000;

							tiempoCompletado += cycleTime; //tiempo completado en segundos
							double tiempoRestante = TOTAL_TIME - tiempoCompletado;
							//if (tiempoRestante) {}	//falta
							std::string st = "Te quedan\n" + std::to_string(int(tiempoRestante)) + "s";
							texto_tiempo->setText(st);

							////////menú de la esquina superior izquierda
							if (labelMenu->c1->clicado) //Empezar de nuevo
								FASE_GENERAL = REINICIAR_PARTIDA;
							if (labelMenu->c2->clicado) //Guardar
								FASE_GENERAL = GUARDAR;
							if (labelMenu->c3->clicado) //Volver a inicio sin guardar
								FASE_GENERAL = MENU_INICIO;
							if (labelMenu->c4->clicado) //Salir sin guardar
								exit(0);

							////////diálogos
							{
								static bool childAdded = false;
								double x = Jack.getX();
								double y = Jack.getY();
								double z = obtenerAltura(x, y, imagen_heightmap, NUM_PUNTOS, SEPARACION_PUNTOS, HEIGHTMAP_ALTURA);
								if(Yo.estaCercaDe(x,y,z)){
									for (int i = 0; i < numDialogosJack; i++) {
										if (dialogosJack[i].fase == FASE_JUEGO && dialogosJack[i].personaje == "Jack" && !(dialogoPersonaje == "Jack" && dialogoFase == dialogosJack[i].fase)) {
											dialogoPersonaje = "Jack";
											dialogoFase = dialogosJack[i].fase;
											boxDialogo->removeWidget(pregunta); //se quita la pregunta de boxDialogo y luego se añade ya que hay que añadir el ultimo primero y viceversa
											//respuesta3
											std::string s = dialogosJack[i].respuesta[2].respuesta;
											if(s.length()==0)
												boxDialogo->removeWidget(respuesta3);
											else {
												respuesta3->setLabel(ajustarString(s, MAX_POR_LINEA));
												boxDialogo->addWidget(respuesta3);
											}
											//respuesta2
											s = dialogosJack[i].respuesta[1].respuesta;
											if(s.length()==0)
												boxDialogo->removeWidget(respuesta2);
											else {
												respuesta2->setLabel(ajustarString(s, MAX_POR_LINEA));
												boxDialogo->addWidget(respuesta2);
											}
											//respuesta1
											s = dialogosJack[i].respuesta[0].respuesta;
											if(s.length()==0)
												boxDialogo->removeWidget(respuesta1);
											else {
												respuesta1->setLabel(ajustarString(s, MAX_POR_LINEA));
												boxDialogo->addWidget(respuesta1);
											}
											//pregunta
											s = dialogoPersonaje + ": " + dialogosJack[i].pregunta.pregunta;
											pregunta->setLabel(ajustarString(s, MAX_POR_LINEA));
											boxDialogo->addWidget(pregunta);
											//a qué fase hay que pasar si se selecciona la respuesta correspondiente
											respuesta1fase = dialogosJack[i].respuesta[0].siguientefase;
											respuesta2fase = dialogosJack[i].respuesta[1].siguientefase;
											respuesta3fase = dialogosJack[i].respuesta[2].siguientefase;
											boxDialogo->resize();
											//break;
										}
									}
									if (childAdded == false) {
										wm->addChild(boxDialogo);
										childAdded = true;
									}
									if (respuesta1->clicado) {
										FASE_JUEGO = respuesta1fase;
										respuesta1->reiniciarClick();
									}
									else if (respuesta2->clicado) {
										FASE_JUEGO = respuesta2fase;
										respuesta2->reiniciarClick();
									}
									else if (respuesta3->clicado) {
										FASE_JUEGO = respuesta3fase;
										respuesta2->reiniciarClick();
									}
								}
								else {
									if (childAdded == true) {
										wm->removeChild(boxDialogo);
										childAdded = false;
									}
								}
								respuesta1->clicado = false;
							}
							///////////

							double var = 45 * PI / 180;
							static unsigned int colision[8];
							for (int i = 0; i < 8; i++) {
								colision[i] = 0;
							}
							//geodeSpheres->removeDrawables(0, 8); //para ver las colisiones
							for (int i = 0; i < 8; i++) {
								osg::LineSegment* segment = new osg::LineSegment();
								segment->set(
									osg::Vec3(Yo.getX(), Yo.getY(), Yo.getZ() + 1),
									osg::Vec3(Yo.getX() + LONGITUD_RAYO*cos(Yo.getDireccion() + i*var), Yo.getY() + LONGITUD_RAYO*sin(Yo.getDireccion() + i*var), Yo.getZ() + 1));
								//geodeSpheres->addDrawable(new osg::ShapeDrawable(new osg::Sphere(osg::Vec3(Yo.getX() + LONGITUD_RAYO*cos(Yo.getDireccion()+i*var), Yo.getY() + LONGITUD_RAYO*sin(Yo.getDireccion()+i*var), Yo.getZ() + 1), 0.05)));  //para ver las colisiones
								osgUtil::IntersectVisitor intersector;
								intersector.addLineSegment(segment);
								grupoEscenario->accept(intersector);
								osgUtil::IntersectVisitor::HitList hitList = intersector.getHitList(segment);
								if (!hitList.empty()) {
									colision[i] = 1;
								}
							}

							Yo.actua(cycleTime, imagen_heightmap, colision);
							posH.set(Yo.getX(), Yo.getY(), Yo.getZ());
							mtras = mtras.translate(posH);
							mrotz = mrotz.rotate(Yo.getDireccion() + PI / 2, osg::Vec3d(0, 0, 1));
							mesc = mtras.scale(.1, .1, .1);
							mtotal = mesc*mrotz*mtras;
							mtH->setMatrix(mtotal);

							calcularPosCamara();
							if (cam_detras && cam_detras_en_cabeza) {//hay que poner el vector con signo contrario
								//coger el vector Yo-Camara, normalizarlo y ponerlo delante de Yo
								double x1 = Yo.getX(); //1 para Yo y 2 para Camara
								double y1 = Yo.getY();
								double z1 = Yo.getZ();
								double x2 = Camara.getX();
								double y2 = Camara.getY();
								double z2 = Camara.getZ();
								double dx = x2 - x1;
								double dy = y2 - y1;
								double dz = z2 - z1;
								double d = sqrt(dx*dx + dy*dy + dz*dz);
								viewer.getCamera()->setViewMatrixAsLookAt(osg::Vec3d(Yo.getX() - dx / d*0.2, Yo.getY() - dy / d*0.2, Yo.getZ() - dz / d*0.2 + 2), //desde dónde miramos
									osg::Vec3d(osg::Vec3d(Yo.getX() - dx / d, Yo.getY() - dy / d, Yo.getZ() - dz / d + 2)),										 //hacia dónde miramos
									osg::Vec3d(0, 0, 1));																									 //vector vertical
							}
							else if (cam_detras)
								viewer.getCamera()->setViewMatrixAsLookAt(osg::Vec3d(Camara.getX(), Camara.getY(), Camara.getZ() + 2), //desde dónde miramos
									osg::Vec3d(Yo.getX(), Yo.getY(), Yo.getZ() + 2),			    //hacia dónde miramos
									osg::Vec3d(0, 0, 1));									    //vector vertical
							else {
								//coger el vector Yo-Camara, normalizarlo y ponerlo delante de Yo
								double x1 = Yo.getX(); //1 para Yo y 2 para Camara
								double y1 = Yo.getY();
								double z1 = Yo.getZ();
								double x2 = Camara.getX();
								double y2 = Camara.getY();
								double z2 = Camara.getZ();
								double dx = x2 - x1;
								double dy = y2 - y1;
								double dz = z2 - z1;
								double d = sqrt(dx*dx + dy*dy + dz*dz);
								viewer.getCamera()->setViewMatrixAsLookAt(osg::Vec3d(Yo.getX() + dx / d*0.2, Yo.getY() + dy / d*0.2, Yo.getZ() + dz / d*0.2 + 2), //desde dónde miramos
									osg::Vec3d(osg::Vec3d(Yo.getX() + dx / d, Yo.getY() + dy / d, Yo.getZ() + dz / d + 2)),								 //hacia dónde miramos
									osg::Vec3d(0, 0, 1));																							 //vector vertical
							}

							Sol.move(cycleTime);
							myLight->setPosition(osg::Vec4(Sol.getX(), Sol.getY(), Sol.getZ(), 1.0f));
							geodeSol->removeDrawables(0, 1);
							geodeSol->addDrawable(new osg::ShapeDrawable(new osg::Sphere(osg::Vec3(1.1*Sol.getX(), 1.1*Sol.getY(), 1.1*Sol.getZ()), 10)));//para ver de dónde viene la luz 

							{///////////////////////////////////////////////////////cambiado
							 //std::stringstream ss;
							 //ss << "x=" << Yo.getX() << ", y=" << Yo.getY() << ", z=" << Yo.getZ();
							 //texto_intro2->setText(ss.str());
								std::string s = "";
								s.append("Yo: x=");
								s.append(std::to_string(Yo.getX()));
								s.append(", y=");
								s.append(std::to_string(Yo.getY()));
								s.append(", z=");
								s.append(std::to_string(Yo.getZ()));
								s.append("\ncolisiones: ");
								for (int i = 0; i < 8; i++) {
									s.append(std::to_string(colision[i]));
								}
								s.append("\ntiempo de ciclo: ");
								s.append(std::to_string(int(cycleTime*1000)));  //a veces da fallos
								s.append("ms");
								texto_intro2->setText(s);
								texto_toast->setText(toast_global.getMessages());
							}

							//texto_intro3->setText(str_coordMouse);



							Sleep(33);
							viewer.frame();

						}
						viewer.removeEventHandler(KeyboardEvent);
						viewer.removeEventHandler(resizeHandler);
						viewer.removeEventHandler(mouseHandler);
						break;
				}
				case REINICIAR_PARTIDA:
				{
						Yo.setPosition(X0, Y0, Z0, DIRECCION0);
						FASE_GENERAL = JUGAR;
						break;
				}
				case SALIR:
				{
						exit(0);
						break;
				}
				case MAPA:
				{
					
					viewer.setUpViewInWindow(screenLength*margin, screenHeight*margin, 256, 256, 0);
						viewer.setSceneData(grupoEscenario);
						viewer.realize();
						viewer.getCamera()->setViewMatrixAsLookAt(osg::Vec3d(0, 0, 485), //desde dónde miramos
							osg::Vec3d(0, 0, 0),										 //hacia dónde miramos
							osg::Vec3d(0, 1, 0));										 //vector vertical
						while (FASE_GENERAL == MAPA && !viewer.done()) {

								Sleep(33);
								viewer.frame();
						}
						break;
				}
				default:
				{
						printf("default del switch\n");
						break;
				}
			}
	}

	return viewer.run();

	return 0;

	}

