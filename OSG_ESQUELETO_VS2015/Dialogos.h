#pragma once

#include <string>

struct Pregunta {
	std::string pregunta;
	std::string da;
	int conrespuesta;
};

struct Respuesta {
	std::string respuesta;
	std::string siguientefase;
	std::string necesario;
	std::string sino;
};

struct Dialogo {
	std::string fase;
	std::string personaje;
	Pregunta pregunta;
	Respuesta respuesta[3];
};