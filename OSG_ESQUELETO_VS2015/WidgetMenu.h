// -*-c++-*- osgWidget - Code by: Jeremy Moles (cubicool) 2007-2008
// $Id: osgwidgetmenu.cpp 66 2008-07-14 21:54:09Z cubicool $

#include <iostream>
#include <osgDB/ReadFile>
#include <osgWidget/Util>
#include <osgWidget/WindowManager>
#include <osgWidget/Box>
#include <osgWidget/Label>

// For now this is just an example, but osgWidget::Menu will later be it's own Window.
// I just wanted to get this out there so that people could see it was possible.

//const unsigned int MASK_2D = 0xF0000000;
//const unsigned int MASK_3D = 0x0F000000;

#define MENU_OPCION1 "EMPEZAR PARTIDA NUEVA"
#define MENU_OPCION2 "CONTINUAR PARTIDA GUARDADA"
#define MENU_OPCION3 "ESTADÍSTICAS"
#define MENU_OPCION4 "CRÉDITOS"
#define MENU_OPCION5 "SALIR"

#define OPCION0 "NULO"
#define OPCION1 "Empezar de nuevo"
#define OPCION2 "Guardar"
#define OPCION3 "Volver sin guardar"
#define OPCION4 "Salir sin guardar"

#include "colores.h"

struct ColorLabel: public osgWidget::Label {
	bool overme = false;
	bool clicado = false;
	ColorLabel(const char* label):
    osgWidget::Label("", "") {
        setFont("./Otros/fonts/times.ttf");
        setFontSize(14);
        setFontColor(COLOR_NEGRO);
        setColor(COLOR_GRIS);
        addHeight(25.0f);
        setCanFill(true);
        setLabel(label);
		setAlignVertical(osgWidget::Widget::VerticalAlignment::VA_TOP);
        setEventMask(osgWidget::EVENT_MOUSE_PUSH | osgWidget::EVENT_MASK_MOUSE_MOVE);
    }

    bool mousePush(double, double, const osgWidget::WindowManager*) {
		clicado = true;
		printf("clicado: %s\n", getLabel().c_str());
        return true;
    }

    bool mouseEnter(double, double, const osgWidget::WindowManager*) {
        setColor(COLOR_BLANCO);
		overme = true;
        return true;
    }

    bool mouseLeave(double, double, const osgWidget::WindowManager*) {
        setColor(COLOR_GRIS);
		overme = false;
        return true;
    }
};

class ColorLabelMenu: public ColorLabel {
public:
	osg::ref_ptr<osgWidget::Window> _window;
	ColorLabel* c0;
	ColorLabel* c1;
	ColorLabel* c2;
	ColorLabel* c3;
	ColorLabel* c4;

public:
    ColorLabelMenu(const char* label):
    ColorLabel(label) {
		setFont("./Otros/fonts/dirtydoz.ttf");
		setColor(COLOR_GRIS_CLARITO);
        _window = new osgWidget::Box(
            std::string("Menu_") + label,
            osgWidget::Box::VERTICAL,
            true
        );
		c0 = new ColorLabel(OPCION0);c0->setColor(osg::Vec4(1.0f, 1.0f, 1.0f, 0.0f));
		c1 = new ColorLabel(OPCION1);
		c2 = new ColorLabel(OPCION2);
		c3 = new ColorLabel(OPCION3);
		c4 = new ColorLabel(OPCION4);

        _window->addWidget(c4);
        _window->addWidget(c3);
        _window->addWidget(c2);
        _window->addWidget(c1);
		_window->addWidget(c0);
		
		_window->setAnchorVertical(osgWidget::Window::VA_TOP);
		_window->setAnchorHorizontal(osgWidget::Window::HA_RIGHT);

        _window->resize();

        setColor(0.5f, 0.5f, 1.0f, 0.8f);
    }

	void reiniciarClick() {
		c0->clicado = false;
		c1->clicado = false;
		c2->clicado = false;
		c3->clicado = false;
		c4->clicado = false;
	}

    void managed(osgWidget::WindowManager* wm) {
        osgWidget::Label::managed(wm);

        wm->addChild(_window.get());

        _window->hide();
    }

    void positioned() {
        osgWidget::Label::positioned();
		
        _window->setOrigin(getX(), getHeight());
        _window->resize(getWidth());
    }

    bool mousePush(double, double, const osgWidget::WindowManager*) {
        if(!_window->isVisible())
			_window->show();
        else
			_window->hide();

		/*if (c5->clicado)
			printf("c5 ha sido clicado\n");*/
		
        return true;
    }

    bool mouseLeave(double, double, const osgWidget::WindowManager*) {
		if (!_window->isVisible()) {
			setColor(0.5f, 0.5f, 1.0f, 0.8f);
		}
		/*else {
			bool out = false;
			if(_window->get...)
				out=true
		}*/

        return true;
    }
	bool mouseEnter(double, double, const osgWidget::WindowManager*) {
		setColor(COLOR_VERDE);

		return true;
	}
};

struct MenuLabel : public osgWidget::Label {
	bool clicado = false;
	MenuLabel(const char* label) :
		osgWidget::Label("", "") {
		setFont("./Otros/fonts/Vera.ttf");
		setFontSize(25);
		setFontColor(COLOR_BLANCO);
		setLabel(label);
		setColor(COLOR_AZUL);
		setPadding(10.0f);
		addSize(21.0f, 22.0f);
		addHeight(50.0f);
		addWidth(200.0f);
		setCanFill(true);
		setAlignVertical(osgWidget::Widget::VerticalAlignment::VA_TOP);
		setEventMask(osgWidget::EVENT_MOUSE_PUSH | osgWidget::EVENT_MASK_MOUSE_MOVE);
	}

	bool mousePush(double, double, const osgWidget::WindowManager*) {
		std::string label = getLabel();
		printf("clicado: %s\n", label.c_str());
		clicado = true;
		return true;
	}

	bool mouseEnter(double, double, const osgWidget::WindowManager*) {
		setColor(COLOR_AZUL_CLARITO);
		return true;
	}

	bool mouseLeave(double, double, const osgWidget::WindowManager*) {
		setColor(COLOR_AZUL);
		return true;
	}
};

struct PreguntaLabel : public osgWidget::Label {
	PreguntaLabel(const char* pers, const char* frase, float length, float height) :
		osgWidget::Label("", "") {
		setFont("./Otros/fonts/Vera.ttf");
		setFontSize(15);
		setFontColor(COLOR_AZUL);
		std::string texto(pers);
		texto += ": ";
		texto += frase;
		setLabel(texto);
		setColor(COLOR_NARANJA);
		setPadding(5.0f);
		//addSize(10.0f, 10.0f);
		addHeight(height);
		addWidth(length);
		setCanFill(true);
		setAlignVertical(osgWidget::Widget::VerticalAlignment::VA_BOTTOM);
	}
};

struct RespuestaLabel : public osgWidget::Label {
	bool clicado = false;
	int respuesta = 0;
	RespuestaLabel(int r, const char* texto, float length, float height) :
		osgWidget::Label("", "") {
		respuesta = r;
		setFont("./Otros/fonts/Vera.ttf");
		setFontSize(15);
		setFontColor(COLOR_AZUL);
		setLabel(texto);
		setColor(COLOR_SALMON);
		setPadding(5.0f);
		//addSize(21.0f, 22.0f);
		addHeight(height);
		addWidth(length);
		setCanFill(true);
		setAlignVertical(osgWidget::Widget::VerticalAlignment::VA_BOTTOM);
		setEventMask(osgWidget::EVENT_MOUSE_PUSH | osgWidget::EVENT_MASK_MOUSE_MOVE);
	}

	void reiniciarClick() {
		clicado = false;
	}

	bool mousePush(double, double, const osgWidget::WindowManager*) {
		std::string label = getLabel();
		printf("clicado: %s\n", label.c_str());
		clicado = true;
		return true;
	}

	bool mouseEnter(double, double, const osgWidget::WindowManager*) {
		setColor(COLOR_BLANCO);
		return true;
	}

	bool mouseLeave(double, double, const osgWidget::WindowManager*) {
		setColor(COLOR_SALMON);
		return true;
	}
};
std::string ajustarString(std::string s, int n) {
	std::string r = "";
	for (int k = 0, j = 0; j < s.size(); j++) {
		k++;
		if (s[j] == ' ' && k>n) {
			r += "\n";
			k = 0;
		}
		else
			r += s[j];
	}
	return r;
}

osgWidget::Label* createLabel(const std::string& text) {
	osgWidget::Label* label = new osgWidget::Label("", "");

	label->setFont("./Otros/fonts/Vera.ttf");
	label->setFontSize(25);
	label->setFontColor(COLOR_BLANCO);
	label->setLabel(text);

	label->setPadding(10.0f);
	label->addSize(21.0f, 22.0f);

	label->setColor(COLOR_AZUL);

	/*
	text->setBackdropType(osgText::Text::DROP_SHADOW_BOTTOM_RIGHT);
	text->setBackdropImplementation(osgText::Text::NO_DEPTH_BUFFER);
	text->setBackdropOffset(0.2f);
	*/

	return label;
}
