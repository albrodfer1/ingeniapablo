#pragma once

class Punto {
	protected:
		double x, y, z;
	
	public:
		Punto();
		Punto(double a, double b, double c);
		virtual ~Punto();

		void setX(double a) { x = a; }
		void setY(double b) { y = b; }
		void setZ(double c) { z = c; }
		void setPosition(double a, double b, double c);

		double getX() { return x; }
		double getY() { return y; }
		double getZ() { return z; }

};
