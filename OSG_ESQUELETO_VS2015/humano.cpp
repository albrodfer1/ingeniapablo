#include "humano.h"


Humano::Humano(double a, double b, double c, double teta) : Punto(a, b, c) {
	setDireccion(teta);
	avance = retroceso = giro_izq = giro_der = salto = false;
	velz = 0;
}

/*void humano::girar(double teta)
{
	direccion += teta;
}

void humano::avanzar(double d)
{
	setX(getX() + d*cos(direccion));
	setY(getY() + d*sin(direccion));
}*/

void Humano::startSalto(bool a) {
	if (!getSalto() && a) {
		setVelZ(6);
		setZ(getZ()+0.01);
	}
	setSalto(a);
	if (getAvance())
		setEstadoSalto(1);
	else if (getCorrer())
		setEstadoSalto(2);
	else if (getRetroceso())
		setEstadoSalto(-1);
}

void Humano::setSalto(bool a) {
	salto = a;
}

void Humano::setVolando(bool a) {
	volando = a;
	if (getAvance())
		setEstadoVolando(1);
	else if (getCorrer())
		setEstadoVolando(2);
	else if (getRetroceso())
		setEstadoVolando(-1);
}

void Humano::actua(double var_t, osg::Image* image, unsigned int colision[8])
{
	double vx, vy, vz;
	double x1, y1, z1, z1s;
	double x2, y2, z2, z2s;
	x1 = getX();
	y1 = getY();
	z1 = getZ();
	vz = getVelZ();
	double r=0;
	bool algoDelante = (colision[0] == 1)|| (colision[1] == 1) || (colision[7] == 1);
	bool algoDetras = (colision[4] == 1);
	if (!salto && !volando) {
		if (avance) {
			if (!algoDelante)
				r = 1;
		}
		else if (correr) {
			if (!algoDelante) {
				r = 3;
			}
		}
		else if (retroceso) {
			if (!algoDetras)
				r = -1;
		}
		if (giro_izq)
			direccion += CTE_GIRO;
		else if (giro_der)
			direccion -= CTE_GIRO;
	}
	else {
		int e1 = estado_salto;
		int e2 = estado_volando;
		if (e1==1 || e2==1) {
			if (!algoDelante)
				r = 1;
		}
		else if (e1==2 || e2 == 2) {
			if (!algoDelante) {
				r = 3;
			}
		}
		else if (e1==-1 || e2 == -1) {
			r = -1;
		}
	}
	//peque�o arreglo para que pueda girar estando cerca del suelo
	if (volando && !salto) {
		z1s = obtenerAltura(x1, y1, image, NUM_PUNTOS, SEPARACION_PUNTOS, HEIGHTMAP_ALTURA);
		if ((z1>z1s) && (z1-z1s)<0.5) {
			if (giro_izq)
				direccion += CTE_GIRO;
			else if (giro_der)
				direccion -= CTE_GIRO;
		}
	}
	//calculamos coordenadas y velocidades
	vx = r*VEL_ANDAR*cos(direccion);
	vy = r*VEL_ANDAR*sin(direccion);
	x2 = x1 + vx*var_t;
	y2 = y1 + vy*var_t;
	z1s = obtenerAltura(x1, y1, image, NUM_PUNTOS, SEPARACION_PUNTOS, HEIGHTMAP_ALTURA);
	z2s = obtenerAltura(x2, y2, image, NUM_PUNTOS, SEPARACION_PUNTOS, HEIGHTMAP_ALTURA);
	//calculamos pendiente y factor de reduccion de velocidad
	double d = sqrt( pow(x2-x1,2) + pow(y2-y1,2) );
	double pend;
	if(d!=0) pend = (z2s-z1s)/d;
	else pend = 0;
	double red;
	if (pend >= 0) red = exp(-pend);
	else red = 2 - exp(pend);
	/*if(d!=0) pend = (z2s-z1s)/d;
	else pend = 0;
	double red;
	if(pend<=1) red = 1 - pend;
	else red = 0;*/
	//aplicamos factor de reduccion de velocidad
	r *= red;
	//volvemos a calcular coordenadas y velocidades
	vx = r*VEL_ANDAR*cos(direccion);
	vy = r*VEL_ANDAR*sin(direccion);
	x2 = x1 + vx*var_t;
	y2 = y1 + vy*var_t;
	z1s = obtenerAltura(x1, y1, image, NUM_PUNTOS, SEPARACION_PUNTOS, HEIGHTMAP_ALTURA);
	z2s = obtenerAltura(x2, y2, image, NUM_PUNTOS, SEPARACION_PUNTOS, HEIGHTMAP_ALTURA);
	if (z2s > z1) {
		z2 = z2s;
		setSalto(false);
	}
	else {
		setVolando(true);
		vz = vz + G*var_t;
		z2 = z1 + vz*var_t + 0.5*G*var_t*var_t;
		if (z2 < z2s){
			z2 = z2s;
			vz = 0;
			setVolando(false);
			setSalto(false);
		}
	}
	setX(x2);
	setY(y2);
	setZ(z2);
	setVelZ(vz);
}

void Humano::setDireccion(double teta)
{
	direccion = teta;
}

double Humano::getDireccion()
{
	return direccion;
}

bool Humano::enMovimiento()
{
	if (getAvance() || getCorrer() || getRetroceso())
		return true;
	return false;
}

bool Humano::estaCercaDe(double x, double y, double z)
{
	double hx = getX();
	double hy = getY();
	double hz = getZ();
	
	double d = sqrt( pow(hx-x,2) + pow(hy-y,2) + pow(hz-z,2) );
	
	if (d < DISTANCIA_A_OTROS)
		return true;

	return false;
}

void Humano::setPosition(double a, double b, double c, double d)
{
	Punto::setPosition(a, b, c);
	direccion = d;
}

